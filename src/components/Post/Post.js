import React from 'react';
import { Link } from 'react-router-dom';
import TimeAgo from 'react-timeago';
import ReactPlayer from 'react-player';
import { likePost, postComment, getComments, getReplyComments } from '../../http/http-calls';
import update from 'react-addons-update';
import ContentLoader from "react-content-loader";
import Share from './Share';
import { switchLoader, alertBox } from '../../commonRedux/';

class Post extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            post: props.post,
            shareModel: false,
            currPost: {},
            playing: false,
            showComments: false,
            commentText: '',
            replyCmt: []
        }
        this.likePost = this.likePost.bind(this);
        this.sharePost = this.sharePost.bind(this);
        this.closeShareModal = this.closeShareModal.bind(this);
    }

    postActivityCount = (activityType, action = 'add') => {

        const postObj = this.state.post;
        if (!postObj[activityType]) {
            postObj[activityType] = 0;
        }
        if (action == 'sub') {
            postObj[activityType] = postObj[activityType] - +1;
        } else {
            postObj[activityType] = postObj[activityType] + +1;
        }
        this.setState({ post: postObj });
    }

    likePost = (postId, liked) => {

        this.setState({ post: { ...this.state.post, likeActive: 1 - liked } });
        likePost({ postid: postId }, true)
            .then(async resp => {
                if (resp.message == 'Like') {
                    this.postActivityCount('likesCount');
                } else if (resp.message == 'Dislike') {
                    this.postActivityCount('likesCount', 'sub');
                }
            }, error => {
                console.log('error', error);
            });
    }

    sharePost = (post) => {
        this.setState({ shareModel: true, currPost: post })
    }

    closeShareModal = () => {
        this.setState({ shareModel: false })
    }

    handleOnReady = () => setTimeout(() => this.setState({ playing: true }), 100);

    postCmt = (postid) => {
        switchLoader(true, 'Please wait. Commenting...');
        let obj = { postid, comment: this.state.commentText };
        postComment(obj)
            .then(async resp => {
                this.postActivityCount('cmtsCount');
                const postObj = this.state.post;
                if (!postObj.comment) {
                    postObj.comment = [];
                }
                postObj.comment.unshift(resp.comment);
                this.setState({ post: postObj, showComments: true, commentText: '' });
                switchLoader();
            }, error => {
                alertBox(true, error.data.message);
                switchLoader();
            });
    }

    showComments = (postid) => {

        switchLoader(true, 'Please wait. Getting Comments...');
        getComments({ postid }, true)
            .then(async resp => {
                console.log(resp);
                switchLoader();
                this.setState({ post: { ...this.state.post, comment: resp } })
                this.setState({ showComments: true });
            }, error => {
                alertBox(true, error.data.message);
                switchLoader();
            });
    }

    handleChange = (evt, index) => {
        console.log('evt', evt.target.name, evt.target.value, index);
        const { name, value } = evt.target;
        if (evt.target.name != 'replyCmt') {
            this.setState({ [name]: value }, () => { });
        } else {
            this.setState({ [`${name}`]: { ...this.state.replyCmt, [index]: value } }, () => {
                console.log(this.state);
            });
        }
    }

    shareSuccess = () => {
        this.postActivityCount('shareCount');
    }

    showCommentReply = (commentId) => {
        let index = this.state.post.comment.findIndex(el => el._id === commentId);
        const tempPostData = this.state.post;
        tempPostData.comment[index].showReply = true;
        this.setState({post:tempPostData});
        /* this.setState({
            post: { comment: update(this.state.post.comment, { [index]: { showReply: { $set: true } } }) }
        }, () => {
            console.log('sf', this.state);
        }); */
    }

    showCommentReplies = (commentId) => {

        switchLoader(true, 'Please wait. Getting Comments...');
        getReplyComments({ commentId }, true)
            .then(async resp => {
                switchLoader();
                let index = this.state.post.comment.findIndex(el => el._id === commentId);
                const tempPostData = this.state.post;
                tempPostData.comment[index].replies = resp;
                this.setState({post:tempPostData});
            }, error => {
                alertBox(true, error.data.message);
                switchLoader();
            });
    }
    callBackCommentReply = (replyData,commentIndex) => {
        let postObj = this.state.post;
        if(postObj.comment && postObj.comment[commentIndex]){
            if(postObj.comment[commentIndex].replies){
                postObj.comment[commentIndex].replies.push(replyData);
            }else{
                postObj.comment[commentIndex].replies = [replyData];
            }
            let tempReplyCmt = this.state.replyCmt;
            tempReplyCmt[commentIndex] = '';
            this.setState({ post: postObj,replyCmt:tempReplyCmt});
        }
    }
    render() {

        const { post, showComments, commentText, replyCmt } = this.state;

        return (
            
            <div className="row empty-inner-container-with-border post" key={post._id}>
                
                <div className="container">
                    <div className="row">

                        <ul className="list-group w-100 m-0">
                            <li className="list-group-item d-flex justify-content-between align-items-center">
                                <div className="media w-100">
                                    <div className="media-left mr-2">
                                        <a href="#">
                                            <img className="media-object pic" src={require("../../assets/images/communities_image_placeholder.png")}
                                                alt="..." />
                                        </a>
                                    </div>
                                    <div className="media-body">
                                        <p className="media-heading">
                                            {post.userinfo && post.userinfo.name ? post.userinfo.name : ''}</p>
                                        <p className="media-subheading"><TimeAgo date={post.createdAt} /></p>
                                    </div>

                                    <div className="media-right ">
                                        <div className="dropdown pull-right ">
                                            <button data-display="static" type="button" className="btn" data-toggle="dropdown"><img
                                                src={require("../../assets/images/vertical-dots.png")} /></button>
                                            <div className="dropdown-menu hasUpArrow dropdown-menu-right">
                                                <a href="#" className="dropdown-item">
                                                    <img className="mr-1" src={require("../../assets/images/edit-icon.png")} />
                                                    <span className="m-1">Edit</span></a>
                                                <a href="#" className="dropdown-item" data-toggle="modal" data-target="#dropdownHideModal">
                                                    <img className="mr-1" src={require("../../assets/images/hide-visibility-icon.png")} />
                                                    <span className="m-1">Hide</span></a>
                                                <a href="#" className="dropdown-item">
                                                    <img className="mr-1" src={require("../../assets/images/save-icon.png")} />
                                                    <span className="m-1">Save</span></a>
                                                <a href="#" className="dropdown-item" data-toggle="modal" data-target="#dropdownRemoveModal">
                                                    <img className="mr-1" src={require("../../assets/images/remove-icon.png")} />
                                                    <span className="m-1">Remove</span></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>

                            <li className="list-group-item d-flex justify-content-between align-items-center post-content-area pb-0">
                                <div className="container">

                                    {((post.taggedPeople) && (post.taggedPeople.length !== 0)) &&
                                        <div className="row">
                                            <span> With
                                            {post.taggedPeople.map(tagPeople => (
                                                <span className="taggedName">@{tagPeople.name}</span>
                                            ))}
                                            </span>
                                        </div>
                                    }
                                    <div className="row">
                                        <span> {post.text} </span>
                                    </div>
                                    <div className="row">
                                        <span className="set-overlay">
                                            {
                                                post.contents && post.contents[0] && post.contents[0].contenttype == 'Image' &&
                                                <img className="w-100" src={post.contents[0].content_url || require("../../assets/images/post-image@2x.png")} alt="" />
                                            }
                                            {
                                                post.contents && post.contents[0] && post.contents[0].contenttype == 'Video' &&
                                                <ReactPlayer controls={true} url={post.contents[0].content_url || 'https://www.youtube.com/watch?v=ysz5S6PUM-U'} />
                                            }
                                        </span>
                                    </div>
                                </div>
                            </li>

                            <li className="list-group-item d-flex justify-content-between align-items-center create-post-attachments p-1">
                                <div className="col pl-1">
                                    <ul className="list-group list-group-horizontal remove-border m-0">
                                        <li className="list-group-item pl-0">
                                            {post.likesCount > 0 && <span className="m-1 font-weight-bold"> {post.likesCount} </span>}
                                            <span className="">Likes</span>
                                        </li>
                                        <li className="list-group-item pl-0">
                                            {post.shareCount > 0 && <span className="m-1 font-weight-bold"> {post.shareCount} </span>}
                                            <span className="">Shares</span>
                                        </li>
                                        <li className="list-group-item pl-0">
                                            {post.cmtsCount > 0 && <span className="m-1 font-weight-bold"> {post.cmtsCount} </span>}
                                            <span className="">Comments</span>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            {!post.sharedBy && <div>
                                <li className="list-group-item d-flex justify-content-between align-items-center create-post-attachments p-1 horizontal-line-top">
                                    <div className="col pl-1">
                                        <ul className="list-group list-group-horizontal remove-border m-0 mt-1 mb-1">
                                            <li className="list-group-item pl-0 pointer" onClick={(e) => this.likePost(post._id, post.likeActive)}>
                                                <img src={require("../../assets/images/like-icon.png")} />
                                                <span className="m-1">{post.likeActive === 1 ? 'LIKED' : 'LIKE'}</span>
                                            </li>
                                            <li className="list-group-item pl-0 pointer" onClick={(e) => this.sharePost(post)}>
                                                <img src={require("../../assets/images/share-icon.png")} />
                                                <span className="m-1">SHARE</span>
                                            </li>
                                            <li className="list-group-item pl-0 pointer">
                                                <img src={require("../../assets/images/comment-icon.png")} />
                                                <span className="m-1" onClick={() => this.showComments(post._id)}>COMMENT</span>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                                <li className="list-group-item d-flex justify-content-between align-items-center create-post-attachments p-1 horizontal-line-top">
                                    <div className="col pl-1">
                                        <input className="form-control tempcommentInput"
                                            type="text"
                                            placeholder="Add your comment"
                                            name="commentText"
                                            value={commentText}
                                            onChange={(e) => this.handleChange(e)} />
                                        <button className="tempCommentBtn" onClick={() => this.postCmt(post._id)}>COMMENT</button>
                                    </div>
                                </li>
                            </div>}
                            {(showComments) &&
                                post.comment.map((comment, i) => (
                                    <li className="list-group-item d-flex justify-content-between align-items-center create-post-attachments p-1 horizontal-line-top">
                                        <div className="col pl-1">
                                            <div className="media w-100 align-items-center p-1">
                                                <div className="media-left mr-2">
                                                    <a href="#">
                                                        <img style={{ width: '30px', 'border-radius': '100%' }} className="media-object pic" src={require("../../assets/images/communities_image_placeholder.png")}
                                                            alt="..." />
                                                    </a>
                                                </div>
                                                <div className="media-body">
                                                    <p className="media-heading">{comment.userinfo[0] ? comment.userinfo[0].name : comment.userinfo.name}</p>
                                                    <p className="media-subheading">{comment.text}</p>
                                                    <div className="d-flex">
                                                        <span className="mr-2 pointer" onClick={(e) => this.props.commentLike(comment._id)}>Like</span>
                                                        <span className="mr-2 pointer" onClick={(e) => this.showCommentReply(comment._id)}>Reply</span>
                                                        {(comment.repliesCount && comment.repliesCount > 0) ? <span className="mr-2 pointer" onClick={(e) => this.showCommentReplies(comment._id)}>{comment.repliesCount} replies</span> : ''}
                                                        {(comment.likesCount && comment.likesCount > 0) ? <span className="mr-2 pointer" >{comment.likesCount} likes</span> : ''}
                                                        {/* <span className="mr-2">Translate</span> */}
                                                    </div>
                                                </div>
                                            </div>
                                                {comment.replies && comment.replies.map((reply) => (
                                                    <div class="media w-100 align-items-center p-1">
                                                        <li className="list-group-item d-flex justify-content-between align-items-center">
                                                            <div className="col pl-1">
                                                                <div className="media w-100 align-items-center p-1">
                                                                    <div className="media-left mr-2">
                                                                        <a href="#">
                                                                            <img style={{ width: '30px', 'border-radius': '100%' }} className="media-object pic" src={require("../../assets/images/communities_image_placeholder.png")}
                                                                                alt="..." />
                                                                        </a>
                                                                    </div>
                                                                    <div className="media-body">
                                                                        <p className="media-heading">{reply.userinfo ? reply.userinfo.name : reply.userinfo.name}</p>
                                                                        <p className="media-subheading">{reply.text}</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </li>
                                                    </div>
                                                ))}
                                                
                                                {comment.showReply && <div className="col pl-4 mb-2">
                                                    <input className="form-control tempcommentInput"
                                                        type="text"
                                                        placeholder="Add your comment"
                                                        name="replyCmt"
                                                        value={replyCmt[i]}
                                                        onChange={(e) => this.handleChange(e, i)} />
                                                    <button className="tempCommentBtn" onClick={() => this.props.commentReply(comment._id, replyCmt[i],i,this.callBackCommentReply)}>REPLY</button>
                                                </div>
                                                }
                                        </div>
                                    </li>
                                ))}

                        </ul>
                    </div>
                </div>
                {this.state.shareModel &&
                    <Share
                        post={this.state.currPost}
                        shareSuccess={this.shareSuccess}
                        closeShareModal={this.closeShareModal}
                    />}
            </div>
        );
    }
}

export default Post;
