import React from 'react';
import { Link } from 'react-router-dom';
import { getPosts, likeComment, replyComment, likePost, postComment, getComments } from '../../http/http-calls';
import Post from './Post';
import InfiniteScroll from 'react-infinite-scroller';
import ContentLoader from "react-content-loader";
import Share from './Share';
import TimeAgo from 'react-timeago';

class Posts extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            posts: [],
            page: 1,
            limit: 5,
            hasMore: true,
            postLoaded: false,
            showSkeleton: false,
            showComments: false,
            commentText: ''
        }
        this.loadFunc = this.loadFunc.bind(this);
        this.likePost = this.likePost.bind(this);
        this.getPaginatePost = this.getPaginatePost.bind(this);
    }

    componentDidMount() {
        this.getPaginatePost();
    }

    componentWillReceiveProps() {
        if (this.props.latestPost && this.props.latestPost.userid) {
            
            const latestPost = this.props.latestPost;
            const postObj = this.state.posts;
            
            let isExist = postObj.find(item => item._id == latestPost._id)
            if(!(isExist && isExist._id)){
                console.log(isExist,'asdfasdfasdfad');
                postObj.unshift(latestPost);
                this.setState({ posts: [] }, () => {
                    this.setState({ posts: postObj });
                });
            }
        }
    }

    getPaginatePost = () => {
        let userDetails = JSON.parse(localStorage.currentUser);
        getPosts({ userid: userDetails._id, limit: this.state.limit, page: this.state.page }, true)
            .then(async resp => {
                console.log('resp', resp);
                this.setState({
                    posts: [...this.state.posts, ...resp.post],
                    showSkeleton: false,
                    hasMore: resp.post && resp.post.length > 0 ? true : false,
                    postLoaded: true
                });
            }, error => {
                console.log('error', error);
                this.setState({ showSkeleton: false });
            });
    }

    loadFunc = () => {

        if (!this.state.showSkeleton) {
            this.setState({ showSkeleton: true })
            this.setState(prevState => {
                return {
                    ...prevState,
                    page: prevState.page + 1,
                    showSkeleton: true
                }
            }, () => {
                this.getPaginatePost();
            })
        }

    }

    commentLike = (commentId) => {

        console.log("commentId", commentId);
        // this.setState({ post: { ...this.state.post, likeActive: 1 - liked } });
        likeComment({ commentId }, true)
            .then(async resp => {
                if (resp.message == 'Like') {
                    // this.postActivityCount('likesCount');
                } else if (resp.message == 'Dislike') {
                    // this.postActivityCount('likesCount', 'sub');
                }
            }, error => {
                console.log('error', error);
            });
    }

    commentReply = (commentId, comment,commentIndex,callBack) => {
        console.log("commentId", commentId, comment);
        replyComment({ commentId, comment }, true)
            .then(async resp => {
                callBack(resp.comment,commentIndex)
            }, error => {
                console.log('error', error);
            });
    }

    likePost = (postId, liked) => {

        likePost({ postid: postId }, true)
            .then(async resp => {
               
            }, error => {
                console.log('error', error);
            });
    }

    postCmt = (postid) => {

        let obj = { postid, comment: this.state.commentText };
        postComment(obj)
            .then(async resp => {
                
            }, error => {
                console.log('error', error);
            });
    }

    showComments = (postid) => {

        getComments({ postid }, true)
            .then(async resp => {
                console.log(resp);
                
                this.setState({ showComments: true });
            }, error => {
                console.log('error', error);

            });
    }

    handleChange = (evt, index) => {
        console.log('evt', evt.target.name, evt.target.value, index);
        const { name, value } = evt.target;
        if (evt.target.name != 'replyCmt') {
            this.setState({ [name]: value }, () => { });
        } else {
            this.setState({ [`${name}`]: { ...this.state.replyCmt, [index]: value } }, () => {
                console.log(this.state);
            });
        }
    }
    
    render() {
        const { post, showComments, commentText } = this.state;

        return (
            <div>
                <InfiniteScroll
                    pageStart={0}
                    initialLoad={false}
                    loadMore={this.loadFunc}
                    hasMore={this.state.hasMore}
                >
                    {!this.postLoaded && this.state.posts.map(post => (
                        !post.sharedBy ? <Post post={post} commentLike={this.commentLike} commentReply={this.commentReply} /> :
                            <div className="row empty-inner-container-with-border post" key={post._id}>
                                <div className="container">
                                    <div className="row">
                                        <ul className="list-group w-100 m-0">
                                            <li className="list-group-item d-flex justify-content-between align-items-center">
                                                <div className="media w-100">
                                                    <div className="media-left mr-2">
                                                        <a href="#">
                                                            <img className="media-object pic" src={post.sharedUser && post.sharedUser.avatar ? post.sharedUser.avatar : require("../../assets/images/communities_image_placeholder.png")}
                                                                alt="..." />
                                                        </a>
                                                    </div>
                                                    <div className="media-body">
                                                        <p className="media-heading">{post.sharedUser && post.sharedUser.name ? post.sharedUser.name : 'Anonymous User'}</p>
                                                        <p className="media-subheading"><TimeAgo date={post.sharedOn} /></p>
                                                    </div>
                                                </div>
                                            </li>
                                            <li className="list-group-item d-flex justify-content-between align-items-center post-content-area pb-0">
                                                <div className="col-12 mb-2">
                                                    <div className="col p-0">
                                                        <span> {post.sharedText} </span>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="sharedPostContent">
                                                <Post post={post} />
                                            </li>

                                            <li className="list-group-item d-flex justify-content-between align-items-center create-post-attachments p-1">
                                                <div className="col pl-1">
                                                    <ul className="list-group list-group-horizontal remove-border m-0">
                                                        <li className="list-group-item pl-0">
                                                            {post.likesCount > 0 && <span className="m-1 font-weight-bold"> {post.likesCount} </span>}
                                                            <span className="">Likes</span>
                                                        </li>
                                                        <li className="list-group-item pl-0">
                                                            {post.shareCount > 0 && <span className="m-1 font-weight-bold"> {post.shareCount} </span>}
                                                            <span className="">Shares</span>
                                                        </li>
                                                        <li className="list-group-item pl-0">
                                                            {post.cmtsCount > 0 && <span className="m-1 font-weight-bold"> {post.cmtsCount} </span>}
                                                            <span className="">Comments</span>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </li>

                                            <li className="list-group-item d-flex justify-content-between align-items-center create-post-attachments p-1 horizontal-line-top">
                                                <div className="col pl-1">
                                                    <ul className="list-group list-group-horizontal remove-border m-0 mt-1 mb-1">
                                                        <li className="list-group-item pl-0 pointer" onClick={(e) => this.likePost(post._id, post.likeActive)}>
                                                            <img src={require("../../assets/images/like-icon.png")} />
                                                            <span className="m-1">{post.likeActive === 1 ? 'LIKED' : 'LIKE'}</span>
                                                        </li>
                                                        <li className="list-group-item pl-0 pointer" onClick={(e) => this.sharePost(post)}>
                                                            <img src={require("../../assets/images/share-icon.png")} />
                                                            <span className="m-1">SHARE</span>
                                                        </li>
                                                        <li className="list-group-item pl-0 pointer">
                                                            <img src={require("../../assets/images/comment-icon.png")} />
                                                            <span className="m-1" onClick={() => this.showComments(post._id)}>COMMENT</span>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </li>
                                            <li className="list-group-item d-flex justify-content-between align-items-center create-post-attachments p-1 horizontal-line-top">
                                                <div className="col pl-1">
                                                    <input className="form-control tempcommentInput"
                                                        type="text"
                                                        placeholder="Add your comment"
                                                        name="commentText"
                                                        value={commentText}
                                                        onChange={(e) => this.handleChange(e)} />
                                                    <button class="tempCommentBtn" onClick={() => this.postCmt(post._id)}>COMMENT</button>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                    ))}
                </InfiniteScroll>

                {((this.state.posts.length === 0 || this.state.showSkeleton) && !this.state.postLoaded) &&

                    <div>
                        {Array(5)
                            .fill()
                            .map((item, index) => (
                                <div className="row empty-inner-container-with-border post">
                                    <div className="container">
                                        <div className="row">
                                            <ul className="list-group w-100 m-0">
                                                <li className="list-group-item d-flex justify-content-between align-items-center">
                                                    <ContentLoader
                                                        speed={2}
                                                        width={610}
                                                        height={350}
                                                        viewBox="0 0 610 350"
                                                        backgroundColor="#f3f3f3"
                                                        foregroundColor="#ecebeb"
                                                        {...this.props}
                                                    >
                                                        <rect x="48" y="8" rx="3" ry="3" width="88" height="6" />
                                                        <rect x="48" y="26" rx="3" ry="3" width="52" height="6" />
                                                        <rect x="0" y="56" rx="3" ry="3" width="410" height="6" />
                                                        <rect x="0" y="72" rx="3" ry="3" width="380" height="6" />
                                                        <rect x="0" y="88" rx="3" ry="3" width="178" height="6" />
                                                        <circle cx="20" cy="20" r="20" />
                                                        <rect x="0" y="88" rx="3" ry="3" width="900" height="1000" />
                                                    </ContentLoader>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            ))}
                    </div>
                }

                {(this.state.posts.length === 0 && this.state.postLoaded) &&
                    <div>
                        Add friends to see posts
                    </div>
                }
            </div>
        );
    }
}

export default Posts;
