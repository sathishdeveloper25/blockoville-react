import React from 'react';
import { sharePost } from '../../http/http-calls';
import { switchLoader, alertBox } from '../../commonRedux/';

const Share = props => {
    const [post, setPost] = React.useState(props.post);
    const [shareType, setShareType] = React.useState('');
    const [inputText, setInputText] = React.useState("");
    const sharePostFn = () => {
        switchLoader(true, 'Please wait. Your post sharing...!')
        sharePost({ postid: post._id, text: inputText }, true)
            .then(async resp => {
                console.log('resp', resp);
                props.shareSuccess();
                props.closeShareModal();
                switchLoader();
            }, error => {
                console.log('error', error);
                props.closeShareModal();
                switchLoader();
            });
    }
    const onChangeHandler = event => {
        setInputText(event.target.value);
    }
    return (
        <div className="social-wall-share-popup ">
            <div className="modal-content share-popup--content">
                <div className="modal-header d-flex align-items-center">
                    <h5 className="modal-title">Share</h5>
                    <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={props.closeShareModal}>
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div className="modal-body">
                    <ul className="p-0 m-0 w-100">
                        <li className="list-group-item p-0 pt-1">
                            <select className="form-control custom-select" id="" defaultValue={'DEFAULT'} >
                                <option disabled value="DEFAULT"> Share on my wall</option>
                            </select>
                        </li>
                        <li className="list-group-item p-0 pt-2">
                            <textarea name="inputText" id="" cols="30" rows="4" placeholder="Message" value={inputText} onChange={onChangeHandler}></textarea>
                        </li>
                    </ul>
                </div>
                <div className="modal-footer d-flex align-items-center pt-1">
                    <button type="button" className="btn btn-secondary cancel-btn" data-dismiss="modal" onClick={props.closeShareModal}>Cancel</button>
                    <button type="button" className="btn btn-primary yes-btn" onClick={sharePostFn}>Send</button>
                </div>
            </div>
        </div>

    );
}
export default Share;