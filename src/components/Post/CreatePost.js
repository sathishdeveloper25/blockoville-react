import React from 'react';
import { Link } from 'react-router-dom';
import { createPost, getCSRF, getTagUsers } from '../../http/http-calls';
import { image, video, file } from '../../config/constants';
import ReactTags from 'react-tag-autocomplete';
import {switchLoader,alertBox} from '../../commonRedux/';
class CreatePost extends React.Component {

	constructor(props) {
		super(props);
		this.postFileInput = React.createRef();
		this.reactTags = React.createRef()
		this.state = {
			text: '',
			profileImg: '',
			postImgs: [],
			postAccept: '',
			visibility: 'public',
			tags: [],
			toggleTagInput: false,
			suggestions: [],
			displaySelectedItem:[]
		}
		this.handleChangeImage = this.handleChangeImage.bind(this);
		this.handleChange = this.handleChange.bind(this);
		this.onDelete = this.onDelete.bind(this);
	}

	componentDidMount() {

		// getCSRF()
		// 	.then(async resp => {
		// 		console.log('resp', resp);
		// 		localStorage.setItem('csrf', resp._csrf);
		// 	}, error => {
		// 		console.log('error', error);
		// 	});
	}

	enableUpload = (acceptExt) => {
		let accept = acceptExt.join(',');
		this.setState({ postAccept: accept }, () => {
			this.postFileInput.current.click();
		});
	}

	changeVisibility = () => {
		this.setState({
			visibility: this.state.visibility === 'public' ? 'onlyme' : 'public'
		})
	}
	clearCreatPostInputs = () => {
		this.setState({
			text: '',
			profileImg: '',
			postImgs: [],
			postAccept: '',
			displaySelectedItem:[],
			visibility: 'public',
		})
	}
	post = () => {
		console.log('this.state', this.state);
		const formData = new FormData()
		formData.append('postImgs', this.state.postImgs);
		formData.append('text', this.state.text);
		formData.append('subject', 'Blockoville');
		formData.append('taggPeople', JSON.stringify(this.state.tags));
		formData.append('privacy', this.state.visibility);
		switchLoader(true,'Please wait. Posting your thoughts...!')
		createPost(formData)
			.then(async resp => {
				if(resp.success){
					this.props.setState('latestPost',resp.post);
					this.clearCreatPostInputs();
				}
				switchLoader()
			}, error => {
				console.log('error', error);
				switchLoader()
			});
	}

	handleChangeImage(e) {
		this.setState({ postImgs: e.target.files[0] },() => {
			let getExt = (this.state.postImgs.name.split('.')).pop();
			var displaySelectedItem = [];
			if(image.indexOf('.'+getExt) !== -1){
				let imgTag = () => (<img className="displayPostImg" src={URL.createObjectURL(this.state.postImgs)} />);
				displaySelectedItem.push(imgTag());
			}else if(video.indexOf('.'+getExt) !== -1){
				displaySelectedItem.push("1 Video Selected");
			}else if(file.indexOf('.'+getExt) !== -1){
				displaySelectedItem.push("1 File Selected");
			}
			this.setState({displaySelectedItem:displaySelectedItem});
		})
	}

	handleChange = (evt) => {
		console.log('evt', evt);
		const { name, value } = evt.target;
		this.setState({ [name]: value }, () => {
			console.log('this.state', this.state);
		});
	}
	onDelete(i) {
		const tags = this.state.tags.slice(0)
		tags.splice(i, 1)
		this.setState({ tags })
	}
	onAddition(tag) {
		const tags = [].concat(this.state.tags, tag)
		this.setState({ tags })
	}

	toggleTag = () => {
		if(!this.state.toggleTagInput){
			getTagUsers({}, true)
			.then(async resp => {
				console.log('resp', resp);
				this.setState({ suggestions: resp }, () => {
					console.log('object', this.state.suggestions);
				});
			}, error => {
				console.log('error', error);
			});
		}
		this.setState({ toggleTagInput: !this.state.toggleTagInput });
	}
	
	render() {
		const { postAccept, visibility, tags, toggleTagInput,displaySelectedItem } = this.state;

		return (
			<div className="row empty-inner-container-with-border create-post" >
				<div className="container">
					<div className="row">
						<ul className="list-group w-100 m-0">

							<li className="list-group-item d-flex justify-content-between align-items-center">
								<div className="clearfix content-heading w-100">
									<span className="overlay pull-left"><img className=""
										src={require("../../assets/images/create-plus-icon.png")} /></span>
									<textarea
										className="form-control pull-left border-0 p-1"
										placeholder="Create"
										rows="3"
										name="text"
										value={this.state.text}
										onChange={this.handleChange}
									></textarea>
								</div>
							</li>
							{(displaySelectedItem && displaySelectedItem.length > 0 ) &&
								<li className="list-group-item d-flex justify-content-between align-items-center">
									<div className="clearfix content-heading w-100 pl-2">
										{displaySelectedItem[0]}
									</div>
								</li>
							}
							{(tags.length > 0) &&
								<li className="list-group-item d-flex justify-content-between align-items-center">
									<div className="clearfix content-heading w-100 tagedPeople">
										<ul>
											{tags.map((value, index) => {
												return <li onClick={() => this.onDelete(index)} className="btn-light" key={index}>{value.name} <span>X</span> </li>
											})}
										</ul>
									</div>
								</li>
							}

							<li className="list-group-item d-flex justify-content-between align-items-center create-post-attachments p-1">
								<div className="col-8 customWidth72 pl-1 pr-0">
									<ul className="list-group list-group-horizontal remove-border m-0">
										<li className="list-group-item p-2">
											<img className='pointer' onClick={() => this.enableUpload(image)} src={require("../../assets/images/create-camera-icon.png")} />
										</li>
										<li className="list-group-item p-2">
											<img className='pointer' onClick={() => this.enableUpload(video)} src={require("../../assets/images/create-video-icon.png")} />
										</li>
										<li className="list-group-item p-2">
											<img className='pointer' onClick={() => this.enableUpload(file)} src={require("../../assets/images/create-attach-icon.png")} />
										</li>
										<div className='hide'>
											<input type='file' name="file" accept={postAccept} onChange={this.handleChangeImage} ref={this.postFileInput} />
										</div>
										<li className="list-group-item p-2">
											<button type="button" className="btn btn-light btn-sm tag" onClick={() => this.toggleTag()}>
												Tag connection
                      						</button>
										</li>
										{toggleTagInput &&
											<li className="list-group-item p-2">
												<ReactTags
													ref={this.reactTags}
													placeholderText="Add Tags"
													suggestions={this.state.suggestions}
													autoresize={false}
													onAddition={this.onAddition.bind(this)} />
											</li>
										}
									</ul>
								</div>
								<div className="col pl-1 pr-0">
									<ul className="list-group list-group-horizontal pull-right remove-border m-0">

										<li className="list-group-item p-2">
											<img className="pointer" onClick={this.changeVisibility} src={require("../../assets/images/" + (visibility == 'public' ? "create-visibility-icon" : "hide-visibility-icon@2x") + ".png")} />
											<span className="m-1">Visibility</span>
										</li>

										<li className="list-group-item p-2 ">

											<button type="button" className="btn btn-light btn-sm theme-btn" onClick={this.post}>
												Post
                      						</button>
										</li>
									</ul>
								</div>
							</li>
						</ul>

					</div>
				</div>
			</div >
		);
	}
}

export default CreatePost;
