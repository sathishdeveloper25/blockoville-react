import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { getSuggestions, followUser, getPendingRequests, acceptRequest,cancelRequest } from '../http/http-calls';
import { switchLoader, alertBox } from '../commonRedux/';
import ContentLoader from "react-content-loader";

const Suggestion = props => {

  const [users, setUsers] = useState([]);
  const [pending, setPending] = useState([]);
  const [showSkeletonPR, setShowSkeletonPR] = useState(true);
  const [showSkeleton, setShowSkeleton] = useState(true);

  useEffect(() => {
    // switchLoader(true, 'Please wait. Fetching your friends...! ');
    callPendingRequests();
    callSuggestions();
  }, []);

  const callPendingRequests = () => {
    getPendingRequests({}, true)
      .then(async resp => {
        console.log('resp', resp);
        setPending(resp);
        resp.length === 0 ? setShowSkeletonPR(false) : '';
      }, error => {
        switchLoader();
        console.log('error', error);
      });
  }

  const callSuggestions = () => {
    getSuggestions({ limit: 10, page: 1 }, true)
      .then(async resp => {
        //console.log('resp.message', resp.message);
        setUsers(resp.message);
        resp.message.length === 0 ? setShowSkeleton(false) : '';
        switchLoader();
      }, error => {
        switchLoader();
        //console.log('error', error);
      });
  }

  const follow = (id, userIndex) => {
    switchLoader(true, 'Following...! ');
    followUser({ followerid: id }, true)
      .then(async resp => {
        switchLoader();
        let tempUser = users;
        tempUser[userIndex].followDocID = resp._id;
        setUsers([]);
        setUsers(tempUser);
        //console.log('resp', tempUser);
      }, error => {
        switchLoader();
        //console.log('error', error);
      });
  }

  const accept = (id, index) => {
    switchLoader(true, 'Following...! ');
    acceptRequest({ id }, true)
      .then(async resp => {
        switchLoader();
        let tempUser = users;
        tempUser.splice(index, 1);
        setPending(tempUser);
        //console.log('resp', resp);
      }, error => {
        switchLoader();
        //console.log('error', error);
      });
  }

  const cancelFollowRequest = (id,index) => {
    switchLoader(true, 'Cancelling...! ');
    cancelRequest({id}, true)
      .then(async resp => {
        switchLoader();
        let tempUser = users;
        tempUser[index].followDocID = '';
        setUsers([]);
        setUsers(tempUser);
      }, error => {
        switchLoader();
        //console.log('error', error);
      });
  }

  return (
    <div class="row empty-inner-container-with-border myfriends">
      <div class="container">
        <div class="row">
          {(pending.length > 0) &&
            <ul class="list-group w-100">
              <li class="list-group-item d-flex justify-content-between align-items-center header-drak">
                Pending Request
                <span class="badge "></span>
              </li>
            </ul>}
          {pending.map((user, index) => (
            <React.Fragment>
              <ul class="list-group w-100 remove-border">
                <li class="list-group-item ">
                  <div class="media w-100 d-flex align-items-center">
                    <div class="media-left mr-2">
                      <a href="#">
                        <img class="media-object pic circle" src={require("../assets/images/communities_image_placeholder.png")}
                          alt="..." />
                      </a>
                    </div>
                    <div class="media-body">
                      <p class="media-heading">{user.userinfo && user.userinfo.name ? user.userinfo.name : ''}</p>
                      {/* <p class="media-subheading">{user.mutualFriends} Mutual Friends</p> */}
                    </div>
                    <div class="media-right ">
                      <button class="btn button-with-fill follow-btn" onClick={(e) => accept(user._id, index)}>
                        <span class="pull-left"> Accept </span>
                        <span class="pull-right">
                          <svg xmlns="http://www.w3.org/2000/svg" width="9.628" height="5.814" viewBox="0 0 9.628 5.814">
                            <path id="Path_1204" data-name="Path 1204" d="M82.8,179.536l-3.4,3.4-3.4-3.4" transform="translate(-74.586 -178.122)" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" />
                          </svg>
                        </span>
                      </button>
                    </div>
                  </div>
                </li>
              </ul>
            </React.Fragment>
          ))}

          {(pending.length === 0 && showSkeletonPR) &&
            <div>
              <ul class="list-group w-100">
                <li class="list-group-item d-flex justify-content-between align-items-center header-drak">
                  Pending Request
                <span class="badge "></span>
                </li>
              </ul>
              {Array(5)
                .fill()
                .map((item, index) => (
                  <ul class="list-group w-100 remove-border">
                    <li class="list-group-item ">
                      <div class="media w-100 d-flex align-items-center">
                        <ContentLoader
                          speed={2}
                          height={40}
                          viewBox="0 0 380 40"
                          backgroundColor="#f3f3f3"
                          foregroundColor="#ecebeb"
                          {...this.props}
                        >
                          <rect x="48" y="8" rx="3" ry="3" width="88" height="6" />
                          <rect x="48" y="26" rx="3" ry="3" width="52" height="6" />

                          <circle cx="20" cy="20" r="20" />
                        </ContentLoader>
                      </div>
                    </li>
                  </ul>
                ))}
            </div>
          }
        </div>
      </div>
      <div class="container">
        <div class="row">
          <ul class="list-group w-100">
            <li class="list-group-item d-flex justify-content-between align-items-center header-drak">
              Suggestions
              <span class="badge "></span>
            </li>
          </ul>
          {users.map((user, index) => (
            <ul class="list-group w-100 remove-border">
              <li class="list-group-item ">
                <div class="media w-100 d-flex align-items-center">
                  <div class="media-left mr-2">
                    <a href="#">
                      <img class="media-object pic circle" src={require("../assets/images/communities_image_placeholder.png")}
                        alt="..." />
                    </a>
                  </div>
                  <div class="media-body">
                    <p class="media-heading">{user.name}</p>
                    <p class="media-subheading">{user.mutualFriends} Mutual Friends</p>
                  </div>
                  <div class="media-right ">
                    {(user.followDocID && user.followDocID != '') ?
                      <button class="btn button-with-fill follow-btn" onClick={(e) => cancelFollowRequest(user.followDocID, index)}>
                        <span class="pull-left"> Cancel </span>
                        <span class="pull-right">
                          <svg xmlns="http://www.w3.org/2000/svg" width="9.628" height="5.814" viewBox="0 0 9.628 5.814">
                            <path id="Path_1204" data-name="Path 1204" d="M82.8,179.536l-3.4,3.4-3.4-3.4" transform="translate(-74.586 -178.122)" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" />
                          </svg>
                        </span>
                      </button>
                      :
                      <button class="btn button-with-fill follow-btn" onClick={(e) => follow(user._id, index)}>
                        <span class="pull-left"> Follow </span>
                        <span class="pull-right">
                          <svg xmlns="http://www.w3.org/2000/svg" width="9.628" height="5.814" viewBox="0 0 9.628 5.814">
                            <path id="Path_1204" data-name="Path 1204" d="M82.8,179.536l-3.4,3.4-3.4-3.4" transform="translate(-74.586 -178.122)" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" />
                          </svg>
                        </span>
                      </button>
                    }
                  </div>
                </div>
              </li>
            </ul>
          ))}

          {(users.length === 0 && showSkeleton) &&
            <div>
              {Array(5)
                .fill()
                .map((item, index) => (
                  <ul class="list-group w-100 remove-border">
                    <li class="list-group-item ">
                      <div class="media w-100 d-flex align-items-center">
                        <ContentLoader
                          speed={2}
                          height={40}
                          viewBox="0 0 380 40"
                          backgroundColor="#f3f3f3"
                          foregroundColor="#ecebeb"
                          {...this.props}
                        >
                          <rect x="48" y="8" rx="3" ry="3" width="88" height="6" />
                          <rect x="48" y="26" rx="3" ry="3" width="52" height="6" />

                          <circle cx="20" cy="20" r="20" />
                        </ContentLoader>
                      </div>
                    </li>
                  </ul>
                ))}
            </div>
          }

        </div>
      </div>
    </div>
  );
}
export default Suggestion;

