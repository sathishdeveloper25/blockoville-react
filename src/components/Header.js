import React from 'react';
import { Link } from 'react-router-dom';
import { withRouter } from 'react-router';
import {getToken} from '../http/token-interceptor';
if(getToken() !== null){
  require ('../assets/css/style.css');
}

/* if (getToken() !== null) {
  commonStyle()
} */
const LoggedOutView = props => {
  
  if (!props.currentUser) {
    return (
      <ul className="nav navbar-nav pull-xs-right">

        <li className="nav-item">
          <Link to="/" className="nav-link">
            Home
          </Link>
        </li>

        <li className="nav-item">
          <Link to="/login" className="nav-link">
            Sign in
          </Link>
        </li>

        <li className="nav-item">
          <Link to="/register" className="nav-link">
            Sign up
          </Link>
        </li>

      </ul>
    );
  }
  return null;
};

const LoggedInView = props => {
  if (props.currentUser) {
    return (
      <ul className="nav navbar-nav pull-xs-right">

        <li className="nav-item">
          <Link to="/" className="nav-link">
            Home
          </Link>
        </li>

        <li className="nav-item">
          <Link to="/editor" className="nav-link">
            <i className="ion-compose"></i>&nbsp;New Post
          </Link>
        </li>

        <li className="nav-item">
          <Link to="/settings" className="nav-link">
            <i className="ion-gear-a"></i>&nbsp;Settings
          </Link>
        </li>

        <li className="nav-item">
          <Link
            to={`/@${props.currentUser.username}`}
            className="nav-link">
            <img src={props.currentUser.image} className="user-pic" alt={props.currentUser.username} />
            {props.currentUser.username}
          </Link>
        </li>

      </ul>
    );
  }

  return null;
};

class Header extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  logout = () => {
    localStorage.removeItem('jwt');
    localStorage.removeItem('currentUser');
    this.props.history.push('/login');
  }
  render() {
    return (
      <div>
        <nav className="navbar navbar-expand-lg navbar-light bg-light navbar--header-top">
          <div className="container">
            <Link to="/">
              <a className="navbar-brand" ><img src={require("../assets/images/logo.png")} /></a>
            </Link>
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
              aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span className="navbar-toggler-icon"></span>
            </button>

            <div className="collapse navbar-collapse" id="navbarSupportedContent">
              <form className="form-inline my-2 my-lg-0">
                <div className="input-group navbar--search">
                  <input type="text" className="form-control" placeholder="Search" />
                  <div className="input-group-append">
                    <button className="btn btn-secondary" type="button">
                      <img src={require("../assets/images/search-icon.png")} />
                    </button>
                  </div>
                </div>
              </form>
              <ul className="navbar-nav ml-auto">
                <li className="nav-item active">
                  <a className="nav-link" href="#"><img src={require("../assets/images/mail-icon.png")} /></a>
                  <div className="menu-hover-container menu-mail">
                    <div className="container">
                      <div className="row">
                        <p className="heading">
                          Recent
                        </p>
                      </div>
                      <div className="row">
                        <ul className="list-group w-100">
                          <li className="list-group-item active">
                            <div className="media w-100 d-flex align-items-center">
                              <div className="media-left">
                                <a className="nav-link status" href="#"><img
                                  src={require("../assets/images/avatar-placeholder.png")} /><span className="badge online"></span>
                                </a>
                              </div>
                              <div className="media-body">
                                <p className="media-heading"><b>Laurentius</b>Hey honey, when will you come home?</p>
                              </div>
                              <div className="media-right">
                                <p className="media-heading">8:01 PM</p>

                              </div>
                            </div>
                          </li>

                          <li className="list-group-item active">
                            <div className="media w-100 d-flex align-items-center">
                              <div className="media-left">
                                <a className="nav-link status" href="#"><img
                                  src={require("../assets/images/avatar-placeholder.png")} /><span className="badge online"></span>
                                </a>
                              </div>
                              <div className="media-body">
                                <p className="media-heading"><b>Laurentius</b>Tomorrow, I will need your help</p>
                              </div>
                              <div className="media-right">
                                <p className="media-heading">8:01 PM</p>
                              </div>
                            </div>
                          </li>

                          <li className="list-group-item">
                            <div className="media w-100 d-flex align-items-center">
                              <div className="media-left">
                                <a className="nav-link status" href="#"><img
                                  src={require("../assets/images/avatar-placeholder.png")} /><span className="badge"></span>
                                </a>
                              </div>
                              <div className="media-body">
                                <p className="media-heading"><b>Laurentius</b> just followed you</p>
                              </div>
                              <div className="media-right">
                                <p className="media-heading">8:01 PM</p>
                              </div>
                            </div>
                          </li>

                          <li className="list-group-item">
                            <div className="media w-100 d-flex align-items-center">
                              <div className="media-left">
                                <a className="nav-link status" href="#"><img
                                  src={require("../assets/images/avatar-placeholder.png")} /><span className="badge "></span>
                                </a>
                              </div>
                              <div className="media-body">
                                <p className="media-heading"><b>Laurentius</b> just followed you</p>
                              </div>
                              <div className="media-right">
                                <p className="media-heading">8:01 PM</p>
                              </div>
                            </div>
                          </li>
                        </ul>
                      </div>

                      <div className="row d-flex justify-content-center align-items-center">
                        <p className="seemore">
                          See more
                        </p>
                      </div>
                    </div>

                  </div>
                </li>
                <li className="nav-item active">
                  <a className="nav-link" href="#"><img src={require("../assets/images/notification.png")} /><span
                    className="badge badge-light">9</span>
                  </a>

                  <div className="menu-hover-container menu-notification">
                    <div className="container">
                      <div className="row">
                        <p className="heading">
                          New
                        </p>
                      </div>
                      <div className="row">
                        <ul className="list-group w-100">
                          <li className="list-group-item active">
                            <div className="media w-100 d-flex align-items-center">
                              <div className="media-left">
                                <a href="#">
                                  <img className="media-object pic circle"
                                    src={require("../assets/images/communities_image_placeholder.png")} alt="..." />
                                </a>
                              </div>
                              <div className="media-body">
                                <p className="media-heading"><b>Laurentius</b> just followed you</p>
                              </div>
                            </div>
                          </li>
                          <li className="list-group-item active">
                            <div className="media w-100 d-flex align-items-center">
                              <div className="media-left">
                                <a href="#">
                                  <img className="media-object pic circle"
                                    src={require("../assets/images/communities_image_placeholder.png")} alt="..." />
                                </a>
                              </div>
                              <div className="media-body">
                                <p className="media-heading"><b>Laurentius</b> just followed you</p>
                              </div>
                            </div>
                          </li>
                        </ul>
                      </div>
                      <div className="row">
                        <p className="heading w-100 d-flex align-items-center">
                          Earlier
                        </p>
                      </div>
                      <div className="row">
                        <ul className="list-group w-100">
                          <li className="list-group-item ">
                            <div className="media w-100 d-flex align-items-center">
                              <div className="media-left">
                                <a href="#">
                                  <img className="media-object pic circle"
                                    src={require("../assets/images/communities_image_placeholder.png")} alt="..." />
                                </a>
                              </div>
                              <div className="media-body">
                                <p className="media-heading"><b>Laurentius</b> just followed you</p>
                              </div>
                            </div>
                          </li>
                          <li className="list-group-item ">
                            <div className="media w-100 d-flex align-items-center">
                              <div className="media-left">
                                <a href="#">
                                  <img className="media-object pic circle"
                                    src={require("../assets/images/communities_image_placeholder.png")} alt="..." />
                                </a>
                              </div>
                              <div className="media-body">
                                <p className="media-heading"><b>Laurentius</b> just followed you</p>
                              </div>
                            </div>
                          </li>
                        </ul>
                      </div>

                      <div className="row d-flex justify-content-center align-items-center">
                        <p className="seemore">
                          See more
                        </p>
                      </div>
                    </div>

                  </div>
                </li>
                <li className="nav-item active ">
                  <a className="nav-link status" href="#"><img src={require("../assets/images/avatar-placeholder.png")} /><span
                    className="badge online"></span>
                  </a>
                  <div className="menu-hover-container menu-profile">
                    <div className="container">

                      <div className="row">
                        <ul className="list-group w-100">
                          <li className="list-group-item pointer">
                            <div className="media w-100 d-flex align-items-center">
                              <div className="media-left">
                                <Link to="/myprofile">
                                  <a>
                                    <svg _ngcontent-dym-c2="" height="11.279" viewBox="0 0 13.158 11.279" width="13.158"
                                      xmlns="http://www.w3.org/2000/svg">
                                      <g _ngcontent-dym-c2="" id="news_feed" transform="translate(-32 -64)">
                                        <path _ngcontent-dym-c2=""
                                          d="M44.688,64H36.7a.484.484,0,0,0-.47.485v.925h-3A1.237,1.237,0,0,0,32,66.643v6.095a2.546,2.546,0,0,0,2.526,2.541h8.215a2.423,2.423,0,0,0,2.417-2.408v-8.4A.471.471,0,0,0,44.688,64Zm-8.459,2.35v5.639H35V66.914a1.434,1.434,0,0,0-.1-.564Zm-.5,7.5a1.735,1.735,0,0,1-1.19.488,1.6,1.6,0,0,1-1.6-1.6V66.914a.558.558,0,1,1,1.116,0v5.545a.471.471,0,0,0,.47.47h1.689A1.545,1.545,0,0,1,35.73,73.851Zm8.488-.981a1.484,1.484,0,0,1-1.477,1.469h-6.18a2.455,2.455,0,0,0,.608-1.6V64.969h7.049Z"
                                          data-name="Path 1153" id="Path_1153"></path>
                                        <path _ngcontent-dym-c2=""
                                          d="M240.235,136h4.7v1.645h-4.7Zm0,2.585h4.7v.94h-4.7Zm0,1.88h4.7v.94h-4.7Zm4.7,1.88h-4.7s0,.94-.235.94h4.368C244.934,143.284,244.934,142.667,244.934,142.344Z"
                                          data-name="Path 1154" id="Path_1154" transform="translate(-201.891 -69.885)"></path>
                                      </g>
                                    </svg>
                                  </a>
                                </Link>
                              </div>
                              <div className="media-body">
                                <p className="media-heading">My profile</p>

                              </div>
                              <div className="media-right  d-flex  align-items-center">

                              </div>
                            </div>
                          </li>
                          <li className="list-group-item ">
                            <div className="media w-100 d-flex align-items-center">
                              <div className="media-left">
                                <a href="#">
                                  <svg _ngcontent-dym-c2="" height="11.279" viewBox="0 0 13.158 11.279" width="13.158"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <g _ngcontent-dym-c2="" id="news_feed" transform="translate(-32 -64)">
                                      <path _ngcontent-dym-c2=""
                                        d="M44.688,64H36.7a.484.484,0,0,0-.47.485v.925h-3A1.237,1.237,0,0,0,32,66.643v6.095a2.546,2.546,0,0,0,2.526,2.541h8.215a2.423,2.423,0,0,0,2.417-2.408v-8.4A.471.471,0,0,0,44.688,64Zm-8.459,2.35v5.639H35V66.914a1.434,1.434,0,0,0-.1-.564Zm-.5,7.5a1.735,1.735,0,0,1-1.19.488,1.6,1.6,0,0,1-1.6-1.6V66.914a.558.558,0,1,1,1.116,0v5.545a.471.471,0,0,0,.47.47h1.689A1.545,1.545,0,0,1,35.73,73.851Zm8.488-.981a1.484,1.484,0,0,1-1.477,1.469h-6.18a2.455,2.455,0,0,0,.608-1.6V64.969h7.049Z"
                                        data-name="Path 1153" id="Path_1153"></path>
                                      <path _ngcontent-dym-c2=""
                                        d="M240.235,136h4.7v1.645h-4.7Zm0,2.585h4.7v.94h-4.7Zm0,1.88h4.7v.94h-4.7Zm4.7,1.88h-4.7s0,.94-.235.94h4.368C244.934,143.284,244.934,142.667,244.934,142.344Z"
                                        data-name="Path 1154" id="Path_1154" transform="translate(-201.891 -69.885)"></path>
                                    </g>
                                  </svg>
                                </a>
                              </div>
                              <div className="media-body">
                                <p className="media-heading">My courses</p>

                              </div>
                              <div className="media-right  d-flex  align-items-center">
                                <span className="badge badge-light badge-green">9</span>

                              </div>
                            </div>
                          </li>
                          <li className="list-group-item ">
                            <div className="media w-100 d-flex align-items-center">
                              <div className="media-left">
                                <a href="#">
                                  <svg _ngcontent-dym-c2="" height="11.279" viewBox="0 0 13.158 11.279" width="13.158"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <g _ngcontent-dym-c2="" id="news_feed" transform="translate(-32 -64)">
                                      <path _ngcontent-dym-c2=""
                                        d="M44.688,64H36.7a.484.484,0,0,0-.47.485v.925h-3A1.237,1.237,0,0,0,32,66.643v6.095a2.546,2.546,0,0,0,2.526,2.541h8.215a2.423,2.423,0,0,0,2.417-2.408v-8.4A.471.471,0,0,0,44.688,64Zm-8.459,2.35v5.639H35V66.914a1.434,1.434,0,0,0-.1-.564Zm-.5,7.5a1.735,1.735,0,0,1-1.19.488,1.6,1.6,0,0,1-1.6-1.6V66.914a.558.558,0,1,1,1.116,0v5.545a.471.471,0,0,0,.47.47h1.689A1.545,1.545,0,0,1,35.73,73.851Zm8.488-.981a1.484,1.484,0,0,1-1.477,1.469h-6.18a2.455,2.455,0,0,0,.608-1.6V64.969h7.049Z"
                                        data-name="Path 1153" id="Path_1153"></path>
                                      <path _ngcontent-dym-c2=""
                                        d="M240.235,136h4.7v1.645h-4.7Zm0,2.585h4.7v.94h-4.7Zm0,1.88h4.7v.94h-4.7Zm4.7,1.88h-4.7s0,.94-.235.94h4.368C244.934,143.284,244.934,142.667,244.934,142.344Z"
                                        data-name="Path 1154" id="Path_1154" transform="translate(-201.891 -69.885)"></path>
                                    </g>
                                  </svg>
                                </a>
                              </div>
                              <div className="media-body">
                                <p className="media-heading">My jobs</p>

                              </div>
                              <div className="media-right  d-flex  align-items-center">
                                <span className="badge badge-light badge-blue">9</span>

                              </div>
                            </div>
                          </li>
                          <li className="list-group-item ">
                            <div className="media w-100 d-flex align-items-center">
                              <div className="media-left">
                                <a href="#">
                                  <svg _ngcontent-dym-c2="" height="11.279" viewBox="0 0 13.158 11.279" width="13.158"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <g _ngcontent-dym-c2="" id="news_feed" transform="translate(-32 -64)">
                                      <path _ngcontent-dym-c2=""
                                        d="M44.688,64H36.7a.484.484,0,0,0-.47.485v.925h-3A1.237,1.237,0,0,0,32,66.643v6.095a2.546,2.546,0,0,0,2.526,2.541h8.215a2.423,2.423,0,0,0,2.417-2.408v-8.4A.471.471,0,0,0,44.688,64Zm-8.459,2.35v5.639H35V66.914a1.434,1.434,0,0,0-.1-.564Zm-.5,7.5a1.735,1.735,0,0,1-1.19.488,1.6,1.6,0,0,1-1.6-1.6V66.914a.558.558,0,1,1,1.116,0v5.545a.471.471,0,0,0,.47.47h1.689A1.545,1.545,0,0,1,35.73,73.851Zm8.488-.981a1.484,1.484,0,0,1-1.477,1.469h-6.18a2.455,2.455,0,0,0,.608-1.6V64.969h7.049Z"
                                        data-name="Path 1153" id="Path_1153"></path>
                                      <path _ngcontent-dym-c2=""
                                        d="M240.235,136h4.7v1.645h-4.7Zm0,2.585h4.7v.94h-4.7Zm0,1.88h4.7v.94h-4.7Zm4.7,1.88h-4.7s0,.94-.235.94h4.368C244.934,143.284,244.934,142.667,244.934,142.344Z"
                                        data-name="Path 1154" id="Path_1154" transform="translate(-201.891 -69.885)"></path>
                                    </g>
                                  </svg>
                                </a>
                              </div>
                              <div className="media-body">
                                <p className="media-heading">Settings</p>

                              </div>
                              <div className="media-right  d-flex  align-items-center">

                              </div>
                            </div>
                          </li>

                          <li className="list-group-item menu--profile-logout" onClick={this.logout}>
                          <a href="#">
                            <div className="media w-100 d-flex align-items-center">
                              <div className="media-left">
                                  <svg _ngcontent-dym-c2="" height="11.279" viewBox="0 0 13.158 11.279" width="13.158"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <g _ngcontent-dym-c2="" id="news_feed" transform="translate(-32 -64)">
                                      <path _ngcontent-dym-c2=""
                                        d="M44.688,64H36.7a.484.484,0,0,0-.47.485v.925h-3A1.237,1.237,0,0,0,32,66.643v6.095a2.546,2.546,0,0,0,2.526,2.541h8.215a2.423,2.423,0,0,0,2.417-2.408v-8.4A.471.471,0,0,0,44.688,64Zm-8.459,2.35v5.639H35V66.914a1.434,1.434,0,0,0-.1-.564Zm-.5,7.5a1.735,1.735,0,0,1-1.19.488,1.6,1.6,0,0,1-1.6-1.6V66.914a.558.558,0,1,1,1.116,0v5.545a.471.471,0,0,0,.47.47h1.689A1.545,1.545,0,0,1,35.73,73.851Zm8.488-.981a1.484,1.484,0,0,1-1.477,1.469h-6.18a2.455,2.455,0,0,0,.608-1.6V64.969h7.049Z"
                                        data-name="Path 1153" id="Path_1153"></path>
                                      <path _ngcontent-dym-c2=""
                                        d="M240.235,136h4.7v1.645h-4.7Zm0,2.585h4.7v.94h-4.7Zm0,1.88h4.7v.94h-4.7Zm4.7,1.88h-4.7s0,.94-.235.94h4.368C244.934,143.284,244.934,142.667,244.934,142.344Z"
                                        data-name="Path 1154" id="Path_1154" transform="translate(-201.891 -69.885)"></path>
                                    </g>
                                  </svg>
                              </div>
                              <div className="media-body pointer">
                                <p className="media-heading">Log out</p>
                              </div>
                              <div className="media-right  d-flex  align-items-center">
                              </div>
                            </div>
                            </a>
                          </li>




                        </ul>
                      </div>


                    </div>

                  </div>
                </li>
                <li className="nav-item active">
                  <a className="nav-link" href="#"><img src={require("../assets/images/plus-icon.png")} /></a>
                  <div className="menu-hover-container menu-plus">
                    <div className="container">

                      <div className="row">
                        <ul className="list-group w-100">
                          <li className="list-group-item ">

                            <p className="media-heading">Blog article</p>

                          </li>
                          <li className="list-group-item ">

                            <p className="media-heading">Gig</p>

                          </li>
                          <li className="list-group-item ">

                            <p className="media-heading">Event</p>

                          </li>
                          <li className="list-group-item ">

                            <p className="media-heading">Marketplace item</p>

                          </li>
                          <li className="list-group-item ">

                            <p className="media-heading">Course</p>

                          </li>
                          <li className="list-group-item ">

                            <p className="media-heading">Job</p>

                          </li>

                        </ul>
                      </div>
                    </div>
                  </div>

                </li>
                <li className="nav-item active">
                  <a className="nav-link navbar-button" href="#">Start Trading</a>
                </li>
                <li className="nav-item active">
                  <a className="nav-link" href="#"><img src={require("../assets/images/more-menu-icon.png")} /></a>
                </li>
              </ul>
            </div>
          </div>
        </nav>
        <nav className="navbar navbar-light bg-light navbar--header-bottom">
          <div className="container">

            <div className="d-flex justify-content-between">
              <a className="nav-link active" href="#">Social profile</a>
            </div>
            <div className="d-flex justify-content-between">
              <a className="nav-link" href="#">Courses</a>
            </div>
            <div className="d-flex justify-content-between">
              <a className="nav-link" href="#" >Marketplace</a>
            </div>
            <div className="d-flex justify-content-between">
              <a className="nav-link" href="#">Blog</a>
            </div>
            <div className="d-flex justify-content-between">
              <a className="nav-link" href="#">Events</a>
            </div>
            <div className="d-flex justify-content-between">
              <a className="nav-link" href="#">Trending</a>
            </div>
            <div className="d-flex justify-content-between">
              <a className="nav-link" href="#">Groups</a>
            </div>
            <div className="d-flex justify-content-between">
              <a className="nav-link" href="#">Jobs</a>
            </div>
            <div className="d-flex justify-content-between">
              <a className="nav-link" href="#">Gigs</a>
            </div>
            <div className="d-flex justify-content-between">
              <a className="nav-link" href="#">+ More</a>
            </div>

          </div>
        </nav>
      </div>
    );
  }
}

export default withRouter(Header);
