import { Link } from 'react-router-dom';
import React from 'react';
import { getSuggestions } from '../http/http-calls';
import { GetAssetImage } from '../globalFunctions/';
import ContentLoader from "react-content-loader";

class RecommendedFriends extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            users: [],
            toggleSkeleton: false
        };
    }

    componentDidMount() {

        getSuggestions({ limit: 3, page: 1 }, true)
            .then(async resp => {
                console.log('resp', resp);
                this.setState({ users: resp.message, toggleSkeleton: true }, () => {
                    console.log('users 22', this.state);
                });
            }, error => {
                console.log('error', error);
            });
    }

    render() {

        return (
            <div className="row empty-inner-container-with-border myfriends">
                <div className="container">
                    <div className="row">
                        <ul className="list-group w-100">
                            <li className="list-group-item d-flex justify-content-between align-items-center header-drak">
                                Recommended Friends
                                {/* <span className="badge ">{this.state.users.length}</span> */}
                            </li>
                        </ul>

                        {this.state.users.map((user, i) => (
                            i < 3 && <ul className="list-group w-100">
                                <li className="list-group-item">
                                    <div className="media">
                                        <div className="media-left">
                                            <a href="#">
                                                <img className="media-object circle" src={GetAssetImage("myfriends_image_placeholder.png")}
                                                    alt="..." />
                                            </a>
                                        </div>
                                        <div className="media-body">
                                            <p className="media-heading">{user.name}</p>
                                            <p className="media-subheading">Media heading</p>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        ))}

                        {(this.state.users.length === 0 && !this.state.toggleSkeleton) &&
                            <ul className="list-group w-100">
                                <li className="list-group-item">
                                    {Array(3)
                                        .fill()
                                        .map((item, index) => (
                                            <ContentLoader
                                                speed={2}
                                                height={40}
                                                viewBox="0 0 200 40"
                                                backgroundColor="#f3f3f3"
                                                foregroundColor="#ecebeb"
                                                {...this.props}
                                            >
                                                <rect x="48" y="8" rx="3" ry="3" width="88" height="6" />
                                                <rect x="48" y="26" rx="3" ry="3" width="52" height="6" />
                                                <circle cx="20" cy="20" r="20" />
                                            </ContentLoader>
                                        ))}
                                </li>
                            </ul>
                        }

                        <ul className="list-group w-100">
                            <li className="list-group-item d-flex justify-content-between align-items-center p-0">
                                <Link to="/suggestions" >
                                    <a className="btn btn-secondary dropdown-toggle w-100 seemore">See more</a>
                                </Link>
                            </li>
                        </ul>

                    </div>
                </div>
            </div>
        )
    }
}

export default RecommendedFriends;