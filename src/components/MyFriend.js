import { Link } from 'react-router-dom';
import React from 'react';
import { getTagUsers,unFollowUserRequest } from '../http/http-calls';
import { switchLoader, alertBox } from '../commonRedux/';
const MyFriend = props => {
    const [users, setUsers] = React.useState([]);
    React.useEffect(() => {
        getFriendList();
    }, []);
    const getFriendList = () => {
        getTagUsers({}, true)
        .then(async resp => {
            console.log('resp', resp);
            setUsers(resp);
        }, error => {
            console.log('error', error);
        });
    }
    const unfollow = (id,index) => {
        switchLoader(true, 'un following...! ');
        unFollowUserRequest({ id }, true)
        .then(async resp => {
            switchLoader();
            let tempUser = users;
            tempUser.splice(index, 1);
            setUsers([]);
            setUsers(tempUser);
            //console.log('resp', resp);
        }, error => {
            switchLoader();
            //console.log('error', error);
        });
    }
    return (
        <div class="row empty-inner-container-with-border myfriends">

            <div class="container">
                <div class="row">

                    <ul class="list-group w-100">
                        <li class="list-group-item d-flex justify-content-between align-items-center header-drak">
                            My Friends
                        <span class="badge "></span>
                        </li>
                    </ul>

                    {users.map((user,index) => (

                        <ul class="list-group w-100 remove-border">

                            <li class="list-group-item ">

                                <div class="media w-100 d-flex align-items-center">
                                    <div class="media-left mr-2">
                                        <a href="#">
                                            <img class="media-object pic circle" src={require("../assets/images/communities_image_placeholder.png")}
                                                alt="..." />
                                        </a>
                                    </div>
                                    <div class="media-body">
                                        <p class="media-heading">{user.name}</p>
                                        <p class="media-subheading">{user.mutualFriends} Mutual Friends</p>
                                    </div>
                                    <div class="media-right ">
                                        <button class="btn button-with-fill follow-btn" onClick={(e) => unfollow(user._id, index)}>
                                            <span class="pull-left"> UnFollow </span>
                                            <span class="pull-right">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="9.628" height="5.814" viewBox="0 0 9.628 5.814">
                                                    <path id="Path_1204" data-name="Path 1204" d="M82.8,179.536l-3.4,3.4-3.4-3.4" transform="translate(-74.586 -178.122)" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" />
                                                </svg>
                                            </span>
                                        </button>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    ))}

                </div>
            </div>
        </div>
    )
}

export default MyFriend;