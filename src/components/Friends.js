import { Link } from 'react-router-dom';
// import ListErrors from './ListErrors';
import React from 'react';
import { getTagUsers } from '../http/http-calls';
import { GetAssetImage } from '../globalFunctions/';

class Friends extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      users: []
    };
  }

  componentDidMount() {

    getTagUsers({}, true)
      .then(async resp => {
        console.log('resp', resp);
        this.setState({ users: resp }, () => {
          console.log('users 22', this.state.users);
        });
      }, error => {
        console.log('error', error);
      });

  }

  render() {

    return (
      <div className="row empty-inner-container-with-border myfriends">

        <div className="container">
          <div className="row">

            <ul className="list-group w-100">
              <li className="list-group-item d-flex justify-content-between align-items-center header-drak">
                My Friends
              <span className="badge ">{this.state.users.length}</span>
              </li>
            </ul>

            
              {this.state.users.map((user,i) => ( 
                i < 3 && <ul className="list-group w-100">
                  <li className="list-group-item">

                    <div className="media">
                      <div className="media-left">
                        <a href="#">
                          <img className="media-object circle" src={GetAssetImage("myfriends_image_placeholder.png")}
                            alt="..." />
                        </a>
                      </div>
                      <div className="media-body">
                        <p className="media-heading">{user.name}</p>
                        <p className="media-subheading">Media heading</p>
                      </div>
                    </div>
                  </li>
                </ul>
              ))}

            {/* {this.state.users.length > 3 &&  */}
            <ul className="list-group w-100">
              <li className="list-group-item d-flex justify-content-between align-items-center p-0">
                <Link to="/myfriends" >
                  {/* <button className="btn btn-secondary dropdown-toggle w-100 seemore" type="button">
                    See more
                  </button> */}
                  <a className="btn btn-secondary dropdown-toggle w-100 seemore">See more</a>
                </Link>
              </li>
            </ul>
            {/* } */}

            {this.state.users.length === 0 && <ul className="list-group w-100">
              <li className="list-group-item d-flex justify-content-between align-items-center">
                <Link to="/suggestions">Add Friends</Link>
              </li>
            </ul>
            }

          </div>
        </div>
      </div>
    )
  }
}

export default Friends;

