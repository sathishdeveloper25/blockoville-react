import React from 'react';
import { getMyProfile } from '../http/http-calls';

class MyProfile extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            user: {}
        }

    }

    componentDidMount() {

        getMyProfile({}, true)
            .then(async resp => {
                console.log('resp', resp);
            }, error => {
                console.log('error', error);
            });
    }

    render() {

        return (
            <div>
                <p>My Profile</p>
            </div>
        );
    }
}

export default MyProfile;
