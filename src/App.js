import React from 'react';
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom';
import Loadable from 'react-loadable';
import './index.css';
import { createMuiTheme, ThemeProvider, makeStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { APP_LOAD, REDIRECT,ALERTCLOSE,SWITCH_LOADER } from './constants/actionTypes';
import { store } from './store';
import agent from './agent';
import { setCSRF } from './http/http-calls';
import Header from './components/Header';
import Messenger from './pages/Messenger';
import Friends from './components/Friends';
import RouteGuard from './routeGuard/';
import LoadingOverlay from 'react-loading-overlay';
import Alert from './components/Alert';
const mapStateToProps = state => {
  return {
    appLoaded: state.common.appLoaded,
    appName: state.common.appName,
    currentUser: state.common.currentUser,
    redirectTo: state.common.redirectTo,
    loaderState: state.common.loaderState,
    loaderText: state.common.loaderText,
    alertOpen: state.common.alertOpen,
    alertMessage: state.common.alertMessage,
    alertType: state.common.alertType,
    alertClose:state.common.alertClose
  }
};

const mapDispatchToProps = dispatch => ({
  onLoad: (payload, token) =>
    dispatch({ type: APP_LOAD, payload, token, skipTracking: true }),
  onRedirect: () =>
    dispatch({ type: REDIRECT }),
  switchLoaderRoot: (isActive,loaderText) =>
    dispatch({ type: SWITCH_LOADER, isActive: isActive, loaderText: loaderText })
});

const Loading = () => {
  return (<LoadingOverlay
            active={true}
            spinner
            text="Loading.apply.."
            styles={{
              overlay: (base) => ({
                ...base,
                position: 'fixed',
                zIndex:'9999 !important'
              })
            }}
          ></LoadingOverlay>)
};

const Login = Loadable({
  loader: () => import('./pages/Login/'),
  loading: Loading,
});

const Register = Loadable({
  loader: () => import('./pages/Register/'),
  loading: Loading,
});

const SocialWall = Loadable({
  loader: () => import('./pages/SocialWall/'),
  loading: Loading,
});

const Suggestions = Loadable({
  loader: () => import('./pages/Suggestions/'),
  loading: Loading,
});

const MyFriends = Loadable({
  loader: () => import('./pages/MyFriends/'),
  loading: Loading,
});

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: '#FFFFFF',
  },
}));

const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#6BD2E9',
      light: '#333333',
    },
  }
});

class App extends React.Component {
  constructor() {
    super();
    this.state = { isPageLoaded: false }
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.redirectTo) {
      store.dispatch(push(nextProps.redirectTo));
      this.props.onRedirect();
    }
  }

  componentWillMount() {
    //this.props.switchLoaderRoot(true,"Loading...");
    /* const token = window.localStorage.getItem('jwt');
    if (token) {
      agent.setToken(token);
    }
    this.props.onLoad(token ? agent.Auth.current() : null, token); */
  }
  componentDidMount(){
    this.setState({isPageLoaded:true});
    /* setTimeout(() => {
      this.setState({isPageLoaded:true});
    }, 4000); */
    /* if(!localStorage.csrf){
      setCSRF()
        .then(async resp => {
          localStorage.setItem('csrf',resp._csrf);
        }, error => {
          console.log('error', error);
        });
    } */
  }

  render() {
      return (
        <React.Fragment>
          {this.state.isPageLoaded ?
            <React.Fragment>
              <Alert message={this.props.alertMessage}
              severity={this.props.alertType}
              open={this.props.alertOpen}
              handleclose={() => store.dispatch({ type: ALERTCLOSE})}
              />
              <LoadingOverlay
                active={this.props.loaderState}
                spinner
                text={this.props.loaderText}
                styles={{
                  overlay: (base) => ({
                    ...base,
                    position: 'fixed',
                    zIndex:'9999 !important'
                  })
                }}
              >
              <div className="content">
                <Router forceRefresh={true}>
                  <Switch>
                    <RouteGuard exact name="login" path='/login'  render={(props) => <Login {...props} />} />
                    <RouteGuard exact name="register" path='/register' render={(props) => <Register {...props} />} />
                    <RouteGuard exact name="verifyemail" path='/verifyemail/:emailToken' render={(props) => <Login {...props} />} />
                    <React.Fragment>
                      <Header appName={this.props.appName} currentUser={this.props.currentUser} />
                      <RouteGuard name="home" exact path='/' render={(props) => <SocialWall {...props} />} />
                      <RouteGuard name="suggestions" exact path='/suggestions' render={(props) => <Suggestions {...props} />} />
                      <RouteGuard name="messenger" exact path='/messenger' render={(props) => <Messenger {...props} />} />
                      <RouteGuard name="myfriends" exact path='/myfriends' render={(props) => <MyFriends {...props} />} />
                    </React.Fragment>
                    
                  </Switch>
                </Router>
              </div>
              </LoadingOverlay>
            </React.Fragment>
            :
            <LoadingOverlay
                active={true}
                spinner
                text="Loading..."
                styles={{
                  overlay: (base) => ({
                    ...base,
                    position: 'fixed',
                    zIndex:'9999 !important'
                  })
                }}
              ></LoadingOverlay>
          }
        </React.Fragment>
      );
  
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
