import React from 'react';
import { connect } from 'react-redux';
import { store } from '../store';
import {SWITCH_LOADER,ALERTBOX} from '../constants/actionTypes';



const _switchLoader = (isActive=false,loaderText) => {
    store.dispatch({ type: SWITCH_LOADER, isActive: isActive, loaderText: loaderText })
}
const _triggerAlert = (alertOpen=false,message='',type='error') => {
    store.dispatch(
            {type:ALERTBOX,
            alertOpen: alertOpen,
            alertMessage: message,
            alertType: type}
        )    
}
export const switchLoader = _switchLoader;
export const alertBox = _triggerAlert;
//export default connect(mapStateToProps, mapDispatchToProps)(CommonRedux);
