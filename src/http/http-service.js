import { getToken, getCSRF } from "./token-interceptor";
import axios from 'axios';

const structureQueryParams = params => {
    let queryStrings = "?";
    const keys = Object.keys(params);
    keys.forEach((key, index) => {
        queryStrings += key + "=" + params[key];
        if (params[keys[index + 1]]) {
            queryStrings += "&";
        }
    });
    return queryStrings;
};
const setHeaders = () => {
    let headers = {};
    const authToken = getToken();
    /* csrfToken = getCSRF();
    if(csrfToken){
        headers["XSRF-TOKEN"] = csrfToken;
    }  */  
    if (authToken) {
        headers["Authorization"] = "Bearer "+authToken;
        //headers["x-access-token"] = authToken;
    }
    return headers;
}
export const handleErrorIfAvailable = httpResponse => {
    switch (httpResponse.status) {
        case 401: {
            localStorage.clear();
            window.location.reload();
            break;
        }
        default: {

        }
    }
}

export const makeAxiosGetRequest = async (url, attachToken = false, params = null) => {

    let queryString = "";
    if (params) {
        queryString = structureQueryParams(params);
    }

    let headers = { "Content-Type": "application/json" };
    if (attachToken) {
        headers = {...headers,...setHeaders()};
    }
    return axios.get(url + queryString, { headers: headers });
};

export const makeAxiosPostRequest = async (url, attachToken = false, params = {}) => {
    let headers = {
        "Content-Type": "application/json"
    };
    if (attachToken) {
        headers = {...headers,...setHeaders()};
    }
    return axios.post(url, params, { headers: headers });
};

export const makeAxiosPutRequest = async (url, attachToken = false, params = {}) => {
    let headers = {
        "Content-Type": "application/json"
    };
    if (attachToken) {
        headers = {...headers,...setHeaders()};
    }
    return axios.put(url, params, { headers: headers });
};
export const setCSRFRequest = async (url) => {
    axios.get(url) // Send get request to get CSRF token once site is visited.
    .then(res => {
      axios.defaults.headers.post['X-XSRF-TOKEN'] = res.data._csrf; // Set it in header for the rest of the axios requests.
    })
    //return axios.put(url, params, { headers: headers });
};

