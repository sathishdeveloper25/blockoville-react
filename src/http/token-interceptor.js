export const getToken = () => {
    let token = null;
    if (localStorage.jwt) {
        token = localStorage.jwt;
    }
    return token;
};
export const getCSRF = () => {
    let token = null;
    if (localStorage.csrf) {
        token = localStorage.csrf;
    }
    return token;
};