import {
    makeAxiosGetRequest,
    makeAxiosPostRequest,
    makeAxiosPutRequest,
    setCSRFRequest
} from "./http-service";
import config from "../config/index";
import { endPoints } from "../config/end-points";

/**
 * register
 * @param {object} data email and password
 */
export const setCSRF = () => {
    return new Promise((resolve, reject) => {
        setCSRFRequest(config.baseUrl + endPoints.csrf);
        /*  makeAxiosGetRequest(config.baseUrl + endPoints.csrf, false)
             .then(res => {
                 resolve(res.data);
             })
             .catch(e => {
                 console.log("API call error: ", e);
                 reject(e.response);
             }); */
    });
};

/**
 * register
 * @param {object} data email and password
 */
export const register = (data) => {
    return new Promise((resolve, reject) => {
        makeAxiosPostRequest(config.baseUrl + endPoints.register, true, data)
            .then(res => {
                resolve(res.data);
            })
            .catch(e => {
                console.log("API call error: ", e.response);
                reject(e.response);
            });
    });
};

/**
 * login
 * @param {object} data email and password
 */
export const login = (data) => {
    return new Promise((resolve, reject) => {

        makeAxiosPostRequest(config.baseUrl + endPoints.login, true, data)
            .then(res => {
                resolve(res.data);
            })
            .catch(e => {
                console.log("API call error: ", e.response);
                reject(e.response);
            });
    });
};

/**
 * createPost
 * @param {object} data 
 */
export const createPost = (data) => {
    return new Promise((resolve, reject) => {

        makeAxiosPostRequest(config.baseUrl + endPoints.createPost, true, data)
            .then(res => {
                resolve(res.data);
            })
            .catch(e => {
                console.log("API call error: ", e);
                reject(e.response);
            });
    });
};

/**
 * register
 * @param {object} data email and password
 */
export const getPosts = (params, auth) => {
    return new Promise((resolve, reject) => {

        makeAxiosGetRequest(config.baseUrl + endPoints.getPosts, auth, params)
            .then(res => {
                resolve(res.data);
            })
            .catch(e => {
                console.log("API call error: ", e);
                reject(e.response);
            });
    });
};

/**
 * createPost
 * @param {object} data 
 */
export const likePost = (data, auth) => {
    return new Promise((resolve, reject) => {

        makeAxiosPostRequest(config.baseUrl + endPoints.likePost, auth, data)
            .then(res => {
                resolve(res.data);
            })
            .catch(e => {
                console.log("API call error: ", e);
                reject(e.response);
            });
    });
};


/**
 * createPost
 * @param {object} data 
 */
export const sharePost = (data, auth) => {
    return new Promise((resolve, reject) => {

        makeAxiosPostRequest(config.baseUrl + endPoints.sharePost, auth, data)
            .then(res => {
                resolve(res.data);
            })
            .catch(e => {
                console.log("API call error: ", e);
                reject(e.response);
            });
    });
};

/**
 * register
 * @param {object} data email and password
 */
export const getTagUsers = (params, auth) => {
    return new Promise((resolve, reject) => {

        makeAxiosGetRequest(config.baseUrl + endPoints.tagUsers, auth, params)
            .then(res => {
                resolve(res.data);
            })
            .catch(e => {
                console.log("API call error: ", e);
                reject(e.response);
            });
    });
};

/**
 * register
 * @param {object} data email and password
 */
export const getSuggestions = (params, auth) => {
    return new Promise((resolve, reject) => {

        makeAxiosGetRequest(config.baseUrl + endPoints.frndsSuggestion, auth, params)
            .then(res => {
                resolve(res.data);
            })
            .catch(e => {
                console.log("API call error: ", e);
                reject(e.response);
            });
    });
};

/**
 * register
 * @param {object} data email and password
 */
export const getMyProfile = (params, auth) => {
    return new Promise((resolve, reject) => {

        makeAxiosGetRequest(config.baseUrl + endPoints.myProfile, auth, params)
            .then(res => {
                resolve(res.data);
            })
            .catch(e => {
                console.log("API call error: ", e);
                reject(e.response);
            });
    });
};


/**
 * createPost
 * @param {object} data 
 */
export const sendMessage = (data, auth) => {
    return new Promise((resolve, reject) => {

        makeAxiosPostRequest(config.baseUrl + endPoints.sendMsg, auth, data)
            .then(res => {
                resolve(res.data);
            })
            .catch(e => {
                console.log("API call error: ", e);
                reject(e.response);
            });
    });
};

/**
 * register
 * @param {object} data email and password
 */
export const getMessages = (params, auth) => {
    return new Promise((resolve, reject) => {

        makeAxiosGetRequest(`${config.baseUrl}${endPoints.receiveMsg}/${params.id}`, auth, {})
            .then(res => {
                resolve(res.data);
            })
            .catch(e => {
                console.log("API call error: ", e);
                reject(e.response);
            });
    });
};

/**
 * createPost
 * @param {object} data 
 */
export const followUser = (data, auth) => {
    return new Promise((resolve, reject) => {

        makeAxiosGetRequest(config.baseUrl + endPoints.follow, auth, data)
            .then(res => {
                resolve(res.data);
            })
            .catch(e => {
                console.log("API call error: ", e);
                reject(e.response);
            });
    });
};

/**
 * createPost
 * @param {object} data 
 */
export const getPendingRequests = (data, auth) => {
    return new Promise((resolve, reject) => {

        makeAxiosGetRequest(config.baseUrl + endPoints.pendingRequests, auth, data)
            .then(res => {
                resolve(res.data);
            })
            .catch(e => {
                console.log("API call error: ", e);
                reject(e.response);
            });
    });
};

export const acceptRequest = (params, auth) => {
    return new Promise((resolve, reject) => {

        makeAxiosGetRequest(`${config.baseUrl}${endPoints.acceptRequest}/${params.id}`, auth, {})
            .then(res => {
                resolve(res.data);
            })
            .catch(e => {
                console.log("API call error: ", e);
                reject(e.response);
            });
    });
};

export const verifyEmail = (params) => {
    return new Promise((resolve, reject) => {
        makeAxiosGetRequest(`${config.baseUrl}${endPoints.verifyEmail}`, true, params)
            .then(res => {
                resolve(res.data);
            })
            .catch(e => {
                console.log("API call error: ", e);
                reject(e.response);
            });
    });
};

export const postComment = (data, auth = true) => {
    return new Promise((resolve, reject) => {
        makeAxiosPostRequest(config.baseUrl + endPoints.postComment, auth, data)
            .then(res => {
                resolve(res.data);
            })
            .catch(e => {
                console.log("API call error: ", e);
                reject(e.response);
            });
    });
};

export const getComments = (params, auth) => {
    return new Promise((resolve, reject) => {
        makeAxiosGetRequest(`${config.baseUrl}${endPoints.getComments}`, auth, params)
            .then(res => {
                resolve(res.data);
            })
            .catch(e => {
                console.log("API call error: ", e);
                reject(e.response);
            });
    });
};


export const likeComment = (data, auth) => {
    return new Promise((resolve, reject) => {

        makeAxiosPostRequest(config.baseUrl + endPoints.likeComment, auth, data)
            .then(res => {
                resolve(res.data);
            })
            .catch(e => {
                console.log("API call error: ", e);
                reject(e.response);
            });
    });
};

export const replyComment = (data, auth = true) => {
    return new Promise((resolve, reject) => {
        
        makeAxiosPostRequest(config.baseUrl + endPoints.replyComment, auth, data)
            .then(res => {
                resolve(res.data);
            })
            .catch(e => {
                console.log("API call error: ", e);
                reject(e.response);
            });
    });
};

export const getReplyComments = (params, auth) => {
    return new Promise((resolve, reject) => {
        makeAxiosGetRequest(`${config.baseUrl}${endPoints.getReplyComments}`, auth, params)
            .then(res => {
                resolve(res.data);
            })
            .catch(e => {
                console.log("API call error: ", e);
                reject(e.response);
            });
    });
};
export const cancelRequest = (params, auth) => {
    return new Promise((resolve, reject) => {

        makeAxiosGetRequest(`${config.baseUrl}${endPoints.cancelRequest}/${params.id}`, auth, {})
            .then(res => {
                resolve(res.data);
            })
            .catch(e => {
                console.log("API call error: ", e);
                reject(e.response);
            });
    });
};
export const unFollowUserRequest = (params, auth) => {
    return new Promise((resolve, reject) => {

        makeAxiosGetRequest(`${config.baseUrl}${endPoints.unFollowUser}/${params.id}`, auth, {})
            .then(res => {
                resolve(res.data);
            })
            .catch(e => {
                console.log("API call error: ", e);
                reject(e.response);
            });
    });
};
