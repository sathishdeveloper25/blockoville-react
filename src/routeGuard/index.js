import React from 'react';
import { Route,Redirect,useLocation } from 'react-router-dom';
import {getToken} from '../http/token-interceptor';
const routePermission = {
  auth:[
    'suggestions',
    'home',
    'messenger',
    'myfriends',
  ],
  noAuth:[
    'login',
    'register',
    'verifyemail'
  ]
}
const RouteGuard = ({ component: Comp,path, isAuthenticated, ...routeProps }) => {
  
  const tkn = getToken();
  if((routePermission.auth.indexOf(routeProps.name) !== -1 && tkn) || (routePermission.noAuth.indexOf(routeProps.name) !== -1 && !tkn)) {
    return (<Route name={routeProps.name} exact path={path} {...routeProps} />);
  }else if(tkn){
    return (<Redirect to='/' />);
  }else{
    return (<Redirect to='/login' />);
  }
};

export default RouteGuard;