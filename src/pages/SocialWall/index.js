import React from 'react';

import { Link } from 'react-router-dom';
import CreatePost from '../../components/Post/CreatePost';
import Posts from '../../components/Post/Posts';
import {GetAssetImage} from '../../globalFunctions/';
import Friends from '../../components/Friends';
import RecommendedFriends from '../../components/RecommendedFriends';

class SocialWall extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      latestPost: {},
    };
  }
  setStateFunc = (key,value) => {
    this.setState({[key]:value});
  }
  componentDidMount() {

  }
  
    render() {
        return (
            // <!-- Wall container -->
            <div>
            <div className="container my-wall-container">
              <div className="row mt-2">
            
                {/* <!-- left column --> */}
                <div className="col-sm empty-container-with-border left-column">
                  <div className="container">
            
                    {/* <!-- user pic --> */}
                    {/* <div className="row mt-2">
                      <div className="container">
                        <div className="c100 p25">
                          <span><img src={GetAssetImage('avatar.png')} /></span>
                          <div className="slice">
                            <div className="bar"></div>
                            <div className="fill"></div>
                          </div>
                        </div>
                      </div>
                    </div> */}
                    {/* <!-- end user pic --> */}
            
                    {/* <!-- user detail --> */}
                    {/* <div className="row">
                      <div className="container text-center">
                        <p className="h6 profile-title">Felix Arvid Kjellberg</p>
                        <p className="profile-subtitle"><small>Consultant at Google .Inc</small></p>
                      </div>
                    </div> */}
                    {/* <!-- end user detail --> */}
            
                    {/* <!-- follow, mail and bitcoin button --> */}
            
                    {/* <div className="row mt-4 d-flex justify-content-center">
                      <div className="dropdown">
                        <button data-display="static" className="btn button-with-fill follow-btn" data-toggle="dropdown">
                          Follow <img src={GetAssetImage("drop-down-arrow.png")} />
                        </button>
                        <div className="dropdown-menu  dropdown-menu-left">
                          <a href="#" className="dropdown-item">
                    
                            <span className="">Follow</span></a>
                          <a href="#" className="dropdown-item" >
            
                            <span className="">Turn off notifications</span></a>
                          <a href="#" className="dropdown-item">
            
                            <span className="">Unfollow</span></a>
                          <a href="#" className="dropdown-item" >
                          
                            <span className="">Block</span></a>
            
                        </div>
                      </div>
            
                      <button className="btn btn-secondary mail-btn" type="button">
                        <img src={GetAssetImage("profile-mail.png")} />
                      </button>
                      <button className="btn btn-secondary bitcoin-btn" type="button">
                        <img src={GetAssetImage("profile-bitcoin.png")} />
                      </button>
                    </div> */}
                    {/* <!--end follow, mail and bitcoin button --> */}
            
                    {/* <!-- about me --> */}
                    {/* <div className="row mt-4">
                      <div className="container aboutme">
                        <p className="h6">About me <span className="pull-right edit-pencil"><a href="#"><svg id="pencil-edit-button"
                                xmlns="http://www.w3.org/2000/svg" width="9.351" height="9.306" viewBox="0 0 9.351 9.306">
                                <path id="Path_768" data-name="Path 768"
                                  d="M5.815,2.842l1.9,1.9L2.9,9.559,1,7.657Zm3.346-.459-.848-.848a.842.842,0,0,0-1.189,0l-.813.813,1.9,1.9L9.161,3.3A.649.649,0,0,0,9.161,2.383ZM.006,10.33a.216.216,0,0,0,.262.258l2.12-.514-1.9-1.9Z"
                                  transform="translate(-0.001 -1.289)" />
                              </svg>
                            </a></span></p>
                        <p className="text-justify">
                          Hi, I'm Felix Arvid Kjellberg. I know my name is a bit long, but the reason I put the full name here is
                          that it will take a long space to fit this text perfectly here.
                        </p>
                      </div>
                    </div> */}
                    {/* <!-- end about me --> */}
            
                    {/* <!-- Activities --> */}
                    <div className="row mt-4">
                      <div className="container activities">
                        <p className="h6 mb-3">Activities</p>
            
                        <div className="clearfix content-heading">
                          <span className="overlay pull-left">
                            <svg xmlns="http://www.w3.org/2000/svg" width="13.158" height="11.279" viewBox="0 0 13.158 11.279">
                              <g id="news_feed" transform="translate(-32 -64)">
                                <path id="Path_1153" data-name="Path 1153"
                                  d="M44.688,64H36.7a.484.484,0,0,0-.47.485v.925h-3A1.237,1.237,0,0,0,32,66.643v6.095a2.546,2.546,0,0,0,2.526,2.541h8.215a2.423,2.423,0,0,0,2.417-2.408v-8.4A.471.471,0,0,0,44.688,64Zm-8.459,2.35v5.639H35V66.914a1.434,1.434,0,0,0-.1-.564Zm-.5,7.5a1.735,1.735,0,0,1-1.19.488,1.6,1.6,0,0,1-1.6-1.6V66.914a.558.558,0,1,1,1.116,0v5.545a.471.471,0,0,0,.47.47h1.689A1.545,1.545,0,0,1,35.73,73.851Zm8.488-.981a1.484,1.484,0,0,1-1.477,1.469h-6.18a2.455,2.455,0,0,0,.608-1.6V64.969h7.049Z" />
                                <path id="Path_1154" data-name="Path 1154"
                                  d="M240.235,136h4.7v1.645h-4.7Zm0,2.585h4.7v.94h-4.7Zm0,1.88h4.7v.94h-4.7Zm4.7,1.88h-4.7s0,.94-.235.94h4.368C244.934,143.284,244.934,142.667,244.934,142.344Z"
                                  transform="translate(-201.891 -69.885)" />
                              </g>
                            </svg>
                          </span>
                          <p>News Feed <span className="badge notify"></span></p>
                        </div>
            
                        <div className="clearfix content-heading">
                          <span className="overlay pull-left">
                            <svg xmlns="http://www.w3.org/2000/svg" width="14" height="9" viewBox="0 0 14 9">
                              <path id="Group_activities"
                                d="M41.545,115.75a1.875,1.875,0,1,0-1.909-1.875A1.9,1.9,0,0,0,41.545,115.75Zm-5.091,0a1.875,1.875,0,1,0-1.909-1.875A1.9,1.9,0,0,0,36.455,115.75Zm0,1.375c-1.5,0-4.455.719-4.455,2.188V121h9v-1.687C41,117.844,37.95,117.125,36.455,117.125Zm5.091.344A3.4,3.4,0,0,0,41,117.5a1.863,1.863,0,0,1,1,1.812V121h4v-1.687c0-1.469-2.959-1.844-4.455-1.844Z"
                                transform="translate(-32 -112)" />
                            </svg>
            
                          </span>
                          <p>Group <span className="badge"></span></p>
                        </div>
            
                        <div className="clearfix content-heading">
                          <span className="overlay pull-left">
                            <svg xmlns="http://www.w3.org/2000/svg" width="15.278" height="12.5" viewBox="0 0 15.278 12.5">
                              <path id="Courses_activities"
                                d="M8.639,3,1,7.167,3.778,8.681v4.167L8.639,15.5,13.5,12.847V8.681l1.389-.757v4.8h1.389V7.167Zm4.736,4.167L8.639,9.75,3.9,7.167,8.639,4.583Zm-1.264,4.854-3.472,1.9-3.472-1.9V9.437l3.472,1.9,3.472-1.9Z"
                                transform="translate(-1 -3)" />
                            </svg>
            
                          </span>
                          <p>Courses <span className="badge"></span></p>
                        </div>
            
                        <div className="clearfix content-heading">
                          <span className="overlay pull-left">
                            <svg xmlns="http://www.w3.org/2000/svg" width="11.364" height="12.5" viewBox="0 0 11.364 12.5">
                              <path id="Event_activities"
                                d="M12.227,2.136h-.568V1H10.523V2.136H4.841V1H3.7V2.136H3.136A1.14,1.14,0,0,0,2,3.273v9.091A1.14,1.14,0,0,0,3.136,13.5h9.091a1.14,1.14,0,0,0,1.136-1.136V3.273A1.14,1.14,0,0,0,12.227,2.136Zm0,10.227H3.136V4.977h9.091Z"
                                transform="translate(-2 -1)" />
                            </svg>
            
                          </span>
                          <p>Event <span className="badge notify"></span></p>
                        </div>
            
                        <div className="clearfix content-heading">
                          <span className="overlay pull-left">
                            <svg xmlns="http://www.w3.org/2000/svg" width="13.158" height="12.5" viewBox="0 0 13.158 12.5">
                              <path id="Jobs_activities"
                                d="M9.895,4.632V3.316H7.263V4.632ZM3.316,5.947v7.237H13.842V5.947ZM13.842,4.632a1.311,1.311,0,0,1,1.316,1.316v7.237A1.311,1.311,0,0,1,13.842,14.5H3.316A1.311,1.311,0,0,1,2,13.184l.007-7.237A1.306,1.306,0,0,1,3.316,4.632H5.947V3.316A1.311,1.311,0,0,1,7.263,2H9.895a1.311,1.311,0,0,1,1.316,1.316V4.632Z"
                                transform="translate(-2 -2)" />
                            </svg>
            
                          </span>
                          <p>Jobs <span className="badge"></span></p>
                        </div>
            
                        <div className="clearfix content-heading">
                          <span className="overlay pull-left">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16.071" height="12.5" viewBox="0 0 16.071 12.5">
                              <path id="Gigs_activities"
                                d="M3,12.143h7.143v1.786H3Zm0,3.571h7.143V17.5H3ZM3,8.571h7.143v1.786H3ZM3,5h7.143V6.786H3ZM17.286,6.786v8.929H13.714V6.786h3.571M19.071,5H11.929V17.5h7.143Z"
                                transform="translate(-3 -5)" />
                            </svg>
            
                          </span>
                          <p>Gigs <span className="badge"></span></p>
                        </div>
            
                        <div className="clearfix content-heading">
                          <span className="overlay pull-left">
                            <svg xmlns="http://www.w3.org/2000/svg" width="12.505" height="12.5" viewBox="0 0 12.505 12.5">
                              <path id="Marketplace_activities"
                                d="M10.094,8.875a1.244,1.244,0,0,0,1.094-.644l2.237-4.056a.623.623,0,0,0-.544-.925H3.631L3.044,2H1V3.25H2.25L4.5,7.994,3.656,9.519A1.252,1.252,0,0,0,4.75,11.375h7.5v-1.25H4.75l.688-1.25ZM4.225,4.5h7.594L10.094,7.625H5.706ZM4.75,12A1.25,1.25,0,1,0,6,13.25,1.248,1.248,0,0,0,4.75,12ZM11,12a1.25,1.25,0,1,0,1.25,1.25A1.248,1.248,0,0,0,11,12Z"
                                transform="translate(-1 -2)" />
                            </svg>
            
                          </span>
                          <p>Marketplace <span className="badge notify"></span></p>
                        </div>
            
                        <div className="clearfix content-heading">
                          <span className="overlay pull-left">
                            <svg xmlns="http://www.w3.org/2000/svg" width="15" height="12.5" viewBox="0 0 15 12.5">
                              <g id="Blogs_activities" transform="translate(-3 -4)">
                                <g id="Group_413" data-name="Group 413" transform="translate(3 4)">
                                  <path id="Path_1087" data-name="Path 1087" d="M7,16.5h7V4H7ZM8.4,5.667h4.2v9.167H8.4Z"
                                    transform="translate(-3 -4)" />
                                  <rect id="Rectangle_445" data-name="Rectangle 445" width="2" height="8.5"
                                    transform="translate(0 2)" />
                                  <rect id="Rectangle_446" data-name="Rectangle 446" width="2" height="8.5"
                                    transform="translate(13 2)" />
                                </g>
                              </g>
                            </svg>
            
                          </span>
                          <p>Blogs <span className="badge"></span></p>
                        </div>
            
                        <div className="clearfix content-heading">
                          <span className="overlay pull-left"><svg xmlns="http://www.w3.org/2000/svg" width="11.514" height="11.514"
                              viewBox="0 0 11.514 11.514">
                              <path id="Fourms_activities"
                                d="M9.484,3.151v4.03H3.825l-.674.674v-4.7H9.484M10.06,2H2.576A.577.577,0,0,0,2,2.576v8.06l2.3-2.3H10.06a.577.577,0,0,0,.576-.576V2.576A.577.577,0,0,0,10.06,2Zm2.879,2.3H11.787V9.484H4.3v1.151a.577.577,0,0,0,.576.576h6.333l2.3,2.3V4.879A.577.577,0,0,0,12.938,4.3Z"
                                transform="translate(-2 -2)" />
                            </svg>
                          </span>
                          <p>Fourms <span className="badge"></span></p>
                        </div>
            
                      </div>
                    </div>
                    {/* <!-- end Activities --> */}
                  </div>
                </div>
                {/* <!-- end left column --> */}
            
                {/* <!-- center column --> */}
                <div className="col-sm empty-container-with-out-border center-column">
            
                  {/* <!-- create post container --> */}
                  <CreatePost setState={this.setStateFunc} />
                  {/* <!-- end create post container --> */}
            
                  {/* <!-- highlight --> */}
                  {/* <div className="row empty-inner-container-with-out-border highlight">
            
                    <div className="media w-100">
                      <div className="media-left pt-2">
                        <p className="h6 header-drak">Highlight</p>
                      </div>
                      <div className="media-body">
                        <div className="dropdown pull-right ">
                          <button data-display="static" type="button" className="btn" data-toggle="dropdown"><img
                              src={GetAssetImage("horizontal-dots.png")} /></button>
                          <div className="dropdown-menu hasUpArrow dropdown-menu-right">
                            <a href="#" className="dropdown-item">
                              <img className="mr-1" src={GetAssetImage("edit-icon.png")} />
                              <span className="m-1">Edit</span></a>
                            <a href="#" className="dropdown-item" data-toggle="modal" data-target="#dropdownHideModal">
                              <img className="mr-1" src={GetAssetImage("hide-visibility-icon.png")} />
                              <span className="m-1">Hide</span></a>
                            <a href="#" className="dropdown-item">
                              <img className="mr-1" src={GetAssetImage("save-icon.png")} />
                              <span className="m-1">Save</span></a>
                            <a href="#" className="dropdown-item" data-toggle="modal" data-target="#dropdownRemoveModal">
                              <img className="mr-1" src={GetAssetImage("remove-icon.png")} />
                              <span className="m-1">Remove</span></a>
            
                          </div>
                        </div>
                      </div>
                    </div>
            
                    <div className="container empty-inner-container-with-border">
                      <div className="row">
                        <div className="col p-0">
            
                          <div className="card p-2 rounded-0 border-0 bundle">
                            <div className="container p-0">
                              <span className="media-container"><a href="#">
                                  <img src={GetAssetImage("recomended_placeholder.png")} className="card-img-top" alt="..." />
                                </a></span>
                              <span className="media-container"><a href="#">
                                  <img src={GetAssetImage("recomended_placeholder.png")} className="card-img-top" alt="..." />
                                </a></span>
                              <span className="media-container isvideo"><a href="#">
                                  <img src={GetAssetImage("recomended_placeholder.png")} className="card-img-top" alt="..." />
                                  <div className="overlay">
                                    <img src={GetAssetImage("play-icon.png")} alt="" />
                                  </div>
                                </a>
                              </span>
                              <span className="media-container "><a href="#" className="">
                                  <img src={GetAssetImage("recomended_placeholder.png")} className="card-img-top " alt="..." />
            
                                </a></span>
            
                            </div>
            
            
                            <div className="card-body pb-0 pt-2 pl-2 pr-2">
            
                              <div className="media">
                                <div className="media-left">
                                  <p className="media-heading">Hey Guys! I just wanted to say Good morning!</p>
                                  <ul className="list-group list-group-horizontal remove-border">
                                    <li className="list-group-item p-0">
                                      <span className="">06 Feb 2020</span>
                                    </li>
                                    <li className="list-group-item p-0 pl-2">
                                      <img src={GetAssetImage("highligh-comment.png")} />
                                      <span className="m-1 font-weight-bold">29</span>
                                      <span className="">Comments</span>
                                    </li>
            
                                  </ul>
            
                                </div>
                                <div className="media-right ml-auto">
                                  <div className="dropdown pull-right ">
                                    <button data-display="static" type="button" className="btn" data-toggle="dropdown"><img
                                        src={GetAssetImage("highligh-horizontal-dot.png")} /></button>
                                   
                                  </div>
                                </div>
                              </div>
            
                            </div>
                          </div>
            
                        </div>
            
                        <div className="col p-0 vertical-line">
            
                          <div className="card p-2 rounded-0 border-0 h-100">
                            <img src={GetAssetImage("recomended_placeholder.png")} className="card-img-top" alt="..." />
            
                            <div className="card-body pb-0 pt-2 pl-0 pr-0">
                              <div className="container">
                                <div className="row">
                                  <p className="media-heading">Hey Guys! I just wanted to say Good morning and have a nice day! :)))))
                                  </p>
                                  <p className="media-subheading">Hey Guys! I just wanted to say Good morning and have a nice day!
                                    :)))))
                                  </p>
                                </div>
                                <div className="row stick-bottom">
                                  <button type="button" className="btn btn-light btn-sm tag">
                                    6 Application
                                  </button>
                                  <button type="button" className="btn btn-outline-primary theme-btn ml-auto">Apply</button>
                                </div>
                              </div>
            
                            </div>
                          </div>
            
                        </div>
            
            
                        <div className="col p-0 vertical-line">
            
                          <div className="card p-2 rounded-0 border-0 h-100">
                            <img src={GetAssetImage("recomended_placeholder.png")} className="card-img-top" alt="..." />
            
                            <div className="card-body pb-0 pt-2 pl-0 pr-0">
                              <div className="container">
                                <div className="row">
                                  <p className="media-heading">Hey Guys! I just wanted to say Good morning and have a nice day! :)))))
                                  </p>
                                  <p className="media-subheading">Hey Guys! I just wanted to say Good morning and have a nice day!
                                    :)))))
                                  </p>
                                </div>
                                <div className="row stick-bottom">
            
                                  <div className="container">
            
                                    <div className="row">
                                      <p className="media-heading  ml-auto">$ 25</p>
                                    </div>
                                    <div className="row">
                                      <button type="button" className="btn btn-light btn-sm tag-with-out-background">
                                        66 People Purchased
                                      </button>
            
                                      <button type="button" className="btn btn-outline-primary theme-btn ml-auto">Buy</button>
                                    </div>
            
                                  </div>
            
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
            
                  </div> */}
            
                  {/* <!-- end highlight --> */}
            
                  {/* <!-- post container --> */}
                  <Posts setState={this.setStateFunc} latestPost={this.state.latestPost} />
                  {/* <!-- end post container --> */}
            
            
                </div>
                {/* <!-- end center column --> */}
            
                {/* <!--  right column --> */}
                <div className="col-sm empty-container-with-out-border right-column">
                  <Friends />
                  <RecommendedFriends />
                  <div className="row empty-inner-container-with-border communities">
                    <div className="container">
                      <div className="row">
            
                        <ul className="list-group w-100">
            
                          <li className="list-group-item d-flex justify-content-between align-items-center header-drak">
                            Communities
            
                            <button className="btn btn-secondary dropdown-toggle seemore pr-0" type="button">
                              See more
                            </button>
                          </li>
                        </ul>
                        <ul className="list-group w-100">
            
                          <li className="list-group-item">
            
                            <div className="media">
                              <div className="media-left">
                                <a href="#">
                                  <img className="media-object square" src={GetAssetImage("communities_image_placeholder.png")}
                                    alt="..." />
                                </a>
                              </div>
                              <div className="media-body">
                                <p className="media-heading">Media heading</p>
                                <span className="sequence-image"><img src={GetAssetImage("myfriends_image_placeholder.png")} />
                                  <img src={GetAssetImage("myfriends_image_placeholder.png")} />
                                  <img src={GetAssetImage("myfriends_image_placeholder.png")} /></span>
                                <p className="media-subheading">+ 15K <span>like this</span></p>
                              </div>
            
                              <div className="media-right">
                                <a href="#">
                                  <span className="align-middle">Free</span>
                                </a>
                              </div>
                            </div>
                          </li>
            
                          <li className="list-group-item">
            
                            <div className="media">
                              <div className="media-left">
                                <a href="#">
                                  <img className="media-object square" src={GetAssetImage("communities_image_placeholder.png")}
                                    alt="..." />
                                </a>
                              </div>
                              <div className="media-body">
                                <p className="media-heading">Media heading</p>
                                <span className="sequence-image"><img src={GetAssetImage("myfriends_image_placeholder.png")} />
                                  <img src={GetAssetImage("myfriends_image_placeholder.png")} />
                                  <img src={GetAssetImage("myfriends_image_placeholder.png")} /></span>
                                <p className="media-subheading">+ 15K <span>like this</span></p>
                              </div>
            
                              <div className="media-right">
                                <a href="#">
                                  <span className="align-middle">Free</span>
                                </a>
                              </div>
                            </div>
                          </li>
            
                        </ul>
            
            
                      </div>
                    </div>
                  </div>
            
                  <div className="row empty-inner-container-with-border trending">
                    <div className="container">
                      <div className="row">
            
                        <ul className="list-group w-100">
                          <li className="list-group-item d-flex justify-content-between align-items-center border-0 header-grey">
                            Trending
            
                          </li>
                        </ul>
                        <ul className="list-group w-100">
            
                          <li className="list-group-item border-0">
            
                            <div className="media">
                              <div className="media-left">
                                <p className="media-heading">#hiphoplof</p>
                              </div>
                              <div className="media-right ml-auto">
                                <p className="media-subheading">28,000K</p>
                                <div className="progress blue">
                                  <div className="progress-bar" style={{width:'100%'}}></div>
                                </div>
                              </div>
                            </div>
                          </li>
            
                          <li className="list-group-item border-0">
            
                            <div className="media">
                              <div className="media-left">
                                <p className="media-heading">#hiphoplof</p>
                              </div>
                              <div className="media-right ml-auto">
                                <p className="media-subheading">28,000K</p>
                                <div className="progress red">
                                  <div className="progress-bar" style={{width:'30%'}}></div>
                                </div>
                              </div>
                            </div>
                          </li>
            
                          <li className="list-group-item border-0">
            
                            <div className="media">
                              <div className="media-left">
                                <p className="media-heading">#hiphoplof</p>
                              </div>
                              <div className="media-right ml-auto">
                                <p className="media-subheading">28,000K</p>
                                <div className="progress red">
                                  <div className="progress-bar" style={{width:'70%'}}></div>
                                </div>
                              </div>
                            </div>
                          </li>
            
                          <li className="list-group-item border-0">
                            <div className="media">
                              <div className="media-left">
                                <p className="media-heading">#hiphoplof</p>
                              </div>
                              <div className="media-right ml-auto">
                                <p className="media-subheading">28,000K</p>
                                <div className="progress blue">
                                  <div className="progress-bar" style={{width:'10%'}}></div>
                                </div>
                              </div>
                            </div>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                  <div className="row empty-inner-container-with-out-border recomended">
                    <p className="h6 header-drak">Recomended to you</p>
                    <div className="card p-2 rounded-0">
                      <img src={GetAssetImage("recomended_placeholder.png")} className="card-img-top" alt="..." />
            
                      <div className="card-body pb-0 pt-2 pl-0 pr-0">
            
                        <div className="media">
                          <div className="media-left">
                            <p className="media-heading">Cyroto traders club</p>
                            <p className="media-subheading">5444 members</p>
            
                          </div>
                          <div className="media-right ml-auto">
                            <button type="button" className="btn btn-outline-primary">Join</button>
                          </div>
                        </div>
            
                      </div>
                    </div>
                  </div>
            
                </div>
                {/* <!-- end right column --> */}
              </div>
            </div>
            
            {/* <!-- image Modal --> */}
            <div id="myModal" className="modal fade post-image-modal" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel"
              aria-hidden="true">
              <div className="modal-dialog">
                <div className="modal-content">
                  <div className="modal-body">
                    <img src={GetAssetImage("post-image@2x.png")} className="img-responsive" />
                  </div>
                </div>
              </div>
            </div>
            {/* <!-- end image Modal --> */}
            
            
            
            {/* <!-- Hide Modal --> */}
            <div className="modal fade dropdownModal" id="dropdownHideModal" tabIndex="-1" role="dialog"
              aria-labelledby="dropdownHideModalCenterTitle" aria-hidden="true">
              <div className="modal-dialog modal-dialog-centered" role="document">
                <div className="modal-content">
                  <div className="modal-header d-flex align-items-center">
                    <img src={GetAssetImage("plus-icon.png")} alt="" />
                    <h5 className="modal-title" id="dropdownHideModalLongTitle">Confirmation</h5>
                    <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div className="modal-body">
                    <p>Do you really want to see this post ever again?</p>
                  </div>
                  <div className="modal-footer">
                    <button type="button" className="btn btn-secondary cancel-btn" data-dismiss="modal">Cancel</button>
                    <button type="button" className="btn btn-primary yes-btn">Yes</button>
                  </div>
                </div>
              </div>
            </div>
            
            
            {/* <!-- Remove Modal --> */}
            <div className="modal fade dropdownModal" id="dropdownRemoveModal" tabIndex="-1" role="dialog"
              aria-labelledby="dropdownRemoveModalCenterTitle" aria-hidden="true">
              <div className="modal-dialog modal-dialog-centered" role="document">
                <div className="modal-content">
                  <div className="modal-header d-flex align-items-center">
                    <img src={GetAssetImage("plus-icon.png")} alt="" />
                    <h5 className="modal-title" id="dropdownRemoveModalLongTitle">Confirmation</h5>
                    <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div className="modal-body">
                    <p>Do you really want to remove this post?</p>
                  </div>
                  <div className="modal-footer">
                    <button type="button" className="btn btn-secondary cancel-btn" data-dismiss="modal">Cancel</button>
                    <button type="button" className="btn btn-primary yes-btn">Yes</button>
                  </div>
                </div>
              </div>
            </div>
            </div>
            
      )
    }
}

export default SocialWall;
