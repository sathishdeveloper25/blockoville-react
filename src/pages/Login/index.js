import React from 'react';
import "../../assets/css/bootstrap.min.css";
import "../../assets/css/pull-push.css";
import "../../assets/css/fontawesome-all.css";
import "../../assets/css/style2.css";
import "../../assets/css/responsive.css";
import { Link } from 'react-router-dom';
import { login, verifyEmail } from '../../http/http-calls';
import ListErrors from '../../components/Errors/ListErrors';
import { GetAssetImage } from '../../globalFunctions/';
import { switchLoader, alertBox } from '../../commonRedux/';
class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      pwdtype: 'password',
      errors: {}
    };
    this.handleChange = this.handleChange.bind(this);
    this.submitForm = this.submitForm.bind(this);
  }
  
  componentDidMount() {
    if (this.props.match.params.emailToken) {
      switchLoader(true, 'Please wait, Activating your email');
      verifyEmail({ str: this.props.match.params.emailToken })
        .then(async resp => {
          switchLoader();
          alertBox(true, resp.message, 'success');
        }, error => {
          alertBox(true, error.data.message);
          switchLoader();
        });
    }
  }
  handleChange = (evt) => {
    console.log('evt', evt);
    const { name, value } = evt.target;
    this.setState({ [name]: value }, () => { });
  }

  
  handleValidation() {

    let fields = this.state,
      formIsValid = true,
      errors = {};

    if (!fields["email"]) {
      formIsValid = false;
      errors["email"] = "Cannot be empty";
    }

    if (typeof fields["email"] !== "undefined" && fields["email"]) {
      let lastAtPos = fields["email"].lastIndexOf('@');
      let lastDotPos = fields["email"].lastIndexOf('.');
      if (!(lastAtPos < lastDotPos && lastAtPos > 0 && fields["email"].indexOf('@@') == -1 && lastDotPos > 2 && (fields["email"].length - lastDotPos) > 2)) {
        formIsValid = false;
        errors["email"] = "Email is not valid";
      }
    }

    if (!fields["password"]) {
      formIsValid = false;
      errors["password"] = "Cannot be empty";
    }

    this.setState({ errors: errors });
    return formIsValid;
  }

  submitForm = (e) => {

    if (this.handleValidation()) {

      let obj = {
        email: this.state.email,
        password: this.state.password,
        authflag: true
      };
      switchLoader(true, 'Please wait, Validating your credentials...');
      login(obj)
        .then(async resp => {
          localStorage.setItem('currentUser', JSON.stringify(resp.userinfo));
          localStorage.setItem('jwt', resp.token);
          this.props.history.push('/');
          switchLoader();
        }, error => {
          alertBox(true, error.data.message);
          switchLoader();
        });
    } else {
      console.log('Invalid form', this.state.errors);
    }
  }

  render() {
    const { email, name, phone, password, cPassword, pwdtype } = this.state;
    return (
      <React.Fragment>
        <div class="flexCenterBody">
          <main class="mainRow">
            <section class="mainRightSection loginSection">
              <div class="container-fluid">
                <div class="row">

                  <div class="container col-xl-4 col-lg-6 col-md-6 leftSection">
                    <div>
                      <a class="navbar-brand mb-2" href="#">
                        <img src={GetAssetImage('Rectangle_logo.png')} alt="" />
                      </a>
                    </div>
                    <div class="d-flex mb-3">
                      <div class="p-2 flex-fill ">
                        <h2 class="title">Login In</h2>
                        <div class="input-group mb-3">
                          <div class="input-group-append">
                            <span class="input-group-text"><i class="far fa-envelope"></i></span>
                          </div>
                          <input
                            className="form-control"
                            type="email"
                            placeholder="Email"
                            name="email"
                            value={email}
                            onChange={(e) => this.handleChange(e)} />
                          <span style={{ color: "red", width: "100%" }}>{this.state.errors["email"]}</span>

                        </div>
                        <div class="input-group mb-3">
                          <div class="input-group-append">
                            <span class="input-group-text"><i class="fas fa-key"></i></span>
                          </div>

                          <input
                            className="form-control"
                            type={pwdtype}
                            placeholder="Password"
                            name="password"
                            value={password}
                            onChange={(e) => this.handleChange(e)} />
                          <span style={{ color: "red", width: "100%" }}>{this.state.errors["password"]}</span>
                          {(pwdtype == 'password') && <button type="button" className="showPW" onClick={(e) => this.setState({pwdtype: 'text'})}><i className="fa fa-fw fa-eye"></i></button>}
                          {(pwdtype == 'text') && <button type="button" className="showPW" onClick={(e) => this.setState({pwdtype: 'password'})}><i className="fa fa-fw fa-eye-slash"></i></button>}
                        </div>
                        <div class="input-group mb-3">
                          <div class="robotCheck">
                            <label class="customCheck">I'm not a robot
                              <input type="checkbox" checked="checked" />
                              <span class="checkmark"></span>
                            </label>
                          </div>
                        </div>
                        <div class="input-group mb-3 ">
                          <div class="input-group-append nextbtn">
                            <a href="#">Forgot password ?</a>
                            <button
                              className="btn"
                              type="submit"
                              disabled={this.props.inProgress}
                              onClick={(e) => this.submitForm(e)}>
                              Next step
                              </button>
                          </div>
                          <div class="twitter">
                            <button class="btn"><i class="fab fa-twitter"></i> Sign up with Twitter</button>
                          </div>
                          <div class="signUp">
                            <Link to="/register">
                              Don't have an account?<br />Sign Up
                              </Link>
                          </div>
                          <p class="term">By continuing, you're confirming that
                            you've read our<span data-toggle="modal" data-target="#termsPoliciesModal">Terms & Conditions and
                              Cookie Policy</span> </p>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="container-fluid col-xl-8 col-lg-6 col-md-6 bgImg">
                  </div>
                </div>
              </div>
            </section>
          </main>
        </div>
      </React.Fragment>


    );
  }
}

export default Login;
