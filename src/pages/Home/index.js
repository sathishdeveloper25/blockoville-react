import React from 'react';
import Grid from '@material-ui/core/Grid';
import { withRouter } from 'react-router-dom';
import Link from '@material-ui/core/Link';
import moment from 'moment';
import { LinearProgress } from '@material-ui/core';
import { Formik, Form, Field } from 'formik';
import Alert from '../../components/Alert';
import FormField from '../../components/FormField';
import FormButtonBar from '../../components/FormButtonBar';
import Button from '@material-ui/core/Button';
import BuySubscription from '../../components/BuySubscription';
import '../styles.css';
import Box from '@material-ui/core/Box';
import DeleteIcon from '@material-ui/icons/Delete';
import ConfirmBox from '../../components/ConfirmBox';
import PaymentDialog from '../../components/PaymentDialog';
import HttpClient from '../../services/HttpClient';
const httpClient = new HttpClient();

class Home extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      account: {},
      subscription: {},
      alertOpen: false,
      alertMessage: '',
      alertSeverity:'error',
      confirmboxOpen:false,
      confirmboxMsg:"",
      confirmboxFunc:"",
      loaderSubscribeDelete:false,
      isPaymentDialog:false,
      onCardAdded:false,
    };
    this.handleCancelSubscription = this.handleCancelSubscription.bind(this);
  }

  componentDidMount() {

    httpClient.get('/accounts')
      .subscribe(
        resp => {
          this.setState({ account: resp, isLoading: false});
        },
    );

    httpClient.get('/subscriptions')
      .subscribe(
        resp => {
          if(resp && resp.data.length > 0) {
            this.setState({ subscription: resp.data[0], isLoading: false});
          }
        },
    );

    httpClient.get('/appointments')
      .subscribe(
        appointments => {
          var date = new Date();
          var today = date.getTime();
          if(!appointments) appointments = [];
          let upcomingAppointments = appointments.filter(a => a.time > today);
          if(upcomingAppointments.length > 0) {
            upcomingAppointments.sort(function(x, y) {
              return x.time - y.time;
            });
            const nextAppointment = upcomingAppointments[0];
            this.setState({ nextAppointment: nextAppointment, isLoading:false});
          }
        },
        error => {
          this.setState({
            alertOpen:true,
            alertMsg:error,
            alertSeverity:'error'
          })
        },
    );

  }

  handleOpenAlert = (alertMessage,severity) => {
    if(severity == 'success') {
      this.setState({alertSeverity:"success"});
    } else{
      this.setState({alertSeverity:"error"});
    }
    this.setState({ alertOpen: true, alertMessage: alertMessage })
  }

  handleCloseAlert = () => {
    this.setState({ alertOpen: false });
  }

  openConfirmBox = (msg , func) => {
    this.setState({confirmboxMsg:msg,confirmboxOpen:true,confirmboxFunc:func});
  }

  cancelAppointment = (item) => {
    let checkTime = (item) => {
      var currentTime = moment();
      var bookingTime = moment(item.time);
      return bookingTime.diff(currentTime, 'h');
    }
    let _cancelAppointment = (item) => {
      this.setState({confirmboxOpen:false});
      httpClient.delete('/appointments/' + item._id).subscribe(
          resp => {
              this.setState({
                alertOpen:true,
                alertMsg:"Appointment cancelled successfully",
                alertSeverity:'success',
                nextAppointment: {},
              })
            },
            error => {
              this.setState({
                alertOpen:true,
                alertMsg:error,
                alertSeverity:'error'
              })
            },
        );
    }
    var msg = "";
    if(checkTime(item) <= 24){
      msg += "Appointment is in less than 24 hours, only 50% will be refunded.\n ";
    }
    msg = msg + "Are you sure you wish to cancel the appointment?";
    this.setState({confirmboxMsg:msg,confirmboxOpen:true,confirmboxFunc:() => _cancelAppointment(item)});
  }
  enablePaymentDialog = (func) => {
    this.setState({isPaymentDialog:true,onCardAdded:func});
  }

  async handleCancelSubscription() {
    
    this.setState({confirmboxMsg:"",confirmboxOpen:false,confirmboxFunc:"",loaderSubscribeDelete:true});
    const subscription = this.state.subscription;
    httpClient.delete('/subscriptions/' + subscription.id)
      .subscribe(
        async(resp) => {
          this.setState({ subscription: {},loaderSubscribeDelete:false })
          this.handleOpenAlert('Subscription was successfully cancelled', 'success');
        },
        error => {
          this.setState({loaderSubscribeDelete:false})
          this.handleOpenAlert(error.response.data)
        }
    );

  }
  onCompleteSubscription = (subData) => {
    this.setState({ subscription: subData});
    this.handleOpenAlert('Subscription successful', 'success');
  }
  render() {
    const { account, subscription,loaderSubscribeDelete, nextAppointment, confirmboxOpen, confirmboxMsg, confirmboxFunc,isPaymentDialog,onCardAdded } = this.state;
    return (
      
        <Grid container
              className='container'
              direction='column'
              justify='center'
              alignItems='center'>
          
          <ConfirmBox open={confirmboxOpen}
              handleClose = {()=>this.setState({confirmboxOpen:false})}
              proceedFunc = {confirmboxFunc}
              message = {confirmboxMsg}
            />
            <Alert message={this.state.alertMessage}
                 open={this.state.alertOpen}
                 severity={this.state.alertSeverity}
                 handleClose={this.handleCloseAlert}
                 noHide
                 />
                 <PaymentDialog enablePayment={isPaymentDialog} onComplete={onCardAdded} disablePayment = {() => this.setState({isPaymentDialog:false})}/>
          <Grid item sm={10} md={8} lg={6} xl={6} xs={11} className="pageHeight">
            
            { nextAppointment ?
              <Box className="box" p={[2, 2, 2, 2]} mb={2} boxShadow={2}>
                <Grid container direction='row'>
                  <Grid item xs={8} lg={9} sm={8}>
                  
                    <h3>{moment(nextAppointment.time).format('ddd DD MMM HH:mm')} {nextAppointment.name} with {nextAppointment.practitionerName}</h3>
                    <h4>{nextAppointment.locationSpecific === false ? 
                      "Click join to start the appointment" : 
                      nextAppointment.addressLine1 + ', ' + nextAppointment.postcode
                      }
                    </h4>
                  </Grid>
                  <Grid item xs={4} sm={4} lg={3} container direction="row" justify="space-evenly" alignItems="center">
                    {nextAppointment.locationSpecific === false &&  
                    <Button target="_blank" href={nextAppointment.clientLink} size='medium' color='primary' variant='contained' className='submitButton'>Join</Button> }
                    <Button size='medium' color='primary' variant='outlined' className='cancelButton' onClick={() => this.cancelAppointment(nextAppointment)}>Cancel</Button>
                  </Grid>
                </Grid>
              </Box>
            :
              <Box className="box" p={[2, 2, 2, 2]} mb={2} boxShadow={2}>
                <h3>Book an appointment with your private midwife</h3>
                <p>Find the first available time slot in minutes.</p><br />
                <Button
                  variant='contained'
                  color='primary'
                  size='large'
                  className='submitButton'
                  onClick={()=> { this.props.history.push('/appointment') }}
                >
                  Book appointment
                </Button>
              </Box>
            }
            <br />
            { subscription.id ?
              <div>
                <h3>Your subscription</h3>
                {loaderSubscribeDelete && <LinearProgress/>}
                <Box className="box" p={[2, 2, 2, 2]} mb={2} boxShadow={2}>
                  <Grid container direction='row' className='bar'>
                    <Grid item xs={10} className='left' justify='flex-start'>
                      £{subscription.plan.amount_decimal / 100} monthly subscription started at {moment.unix(subscription.created).format("MM/DD/YYYY")}
                    </Grid>
                    <Grid item xs={2} className='right' justify='flex-end'>
                    
                      <DeleteIcon color='primary' className="pointer" onClick={() => {
                        this.openConfirmBox('Are you sure you want to cancel the subscription?', this.handleCancelSubscription)
                      }} />
                    </Grid>
                  </Grid>
                </Box>
              </div>
              
            :

              <div>
                <div className='title'>
                  <h3>Subscribe to our services</h3>
                  Subscribe to benefit for on-going cost-savings and benefits. You can also pay as you go.
                </div>
                <Grid container direction='row' spacing={2} justify="center">
                  <Grid item sm={11} md={6} lg={6} xl={6} xs={11}>
                    <BuySubscription 
                        name='Basic Subscription'
                        price='£120 / month'
                        priceId='price_HOIWZTtG3n7LxD'
                        onComplete={this.onCompleteSubscription}
                        paymentDialog={this.enablePaymentDialog}
                        features={[
                            '4 Tele Clinic appointments per month'
                          ]} />
                  </Grid>
                  <Grid item sm={11} md={6} lg={6} xl={6} xs={11}>
                    <BuySubscription 
                        name='Premium Subscription'
                        price='£300 / month'
                        priceId='price_1H37J9I30bge2lEgpfuD8JFO'
                        onComplete={this.onCompleteSubscription}
                        paymentDialog={this.enablePaymentDialog}
                        features={[
                            '4 Tele Clinic appointments per month',
                            '1 Home Visit appointment per month'
                          ]} />
                  </Grid>
                </Grid>
              </div>

            }

          </Grid>
        </Grid>
    );
  }
}

export default withRouter(Home);
