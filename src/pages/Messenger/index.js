import React from 'react';
import { Link } from 'react-router-dom';
import ListErrors from '../../components/Errors/ListErrors';
import { sendMessage, getMessages } from '../../http/http-calls';
import { GetAssetImage } from '../../globalFunctions/';
import * as moment from 'moment'

class Messenger extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            msg: '',
            msgs: []
        };
        this.handleChange = this.handleChange.bind(this);
        this.sendMsg = this.sendMsg.bind(this);
    }

    componentDidMount() {
        this.getMsg();
    }

    handleChange = (evt) => {
        const { name, value } = evt.target;
        this.setState({ [name]: value }, () => { });
    }

    getMsg = () => {

        getMessages({ id: '5f47840fd605362218221cdf' }, true)
            .then(async resp => {
                console.log('resp', resp);
                this.setState({ msgs: resp });
            }, error => {
                console.log('error', error);
            });
    }

    sendMsg = () => {

        sendMessage({ text: this.state.msg, id: '5f47840fd605362218221cdf' }, true)
            .then(async resp => {
                console.log('resp', resp);
                this.setState({ msg: '' });
            }, error => {
                console.log('error', error);
            });
    }

    render() {

        return (
            <div class="container messanger-container">
                <div class="row mt-2">

                    {/* <!-- left column --> */}
                    <div class="col-sm empty-container-with-out-border left-column">
                        <div class="container  p-0">
                            <div class="row empty-inner-container-with-border chat">
                                <div class="container">
                                    <div class="row">

                                        <ul class="list-group w-100 header ">
                                            <li class="list-group-item d-flex justify-content-between align-items-center horizontal-line-bottom">
                                                CHAT
                                                <button _ngcontent-xal-c1="" class="btn p-0" type="button">
                                                    <img src={GetAssetImage('search.svg')} />
                                                </button>
                                            </li>
                                        </ul>

                                        {/* <ul class="list-group w-100 favourites">
                                            <li class="list-group-item d-flex justify-content-between align-items-center title">
                                                Favourites
                                            </li>
                                            <li class="list-group-item ml-3">
                                                <img class="media-object circle" src={GetAssetImage('avatar-placeholder.png')} />
                                                <p>Daniel</p>
                                            </li>
                                            <li class="list-group-item">
                                                <img class="media-object circle" src={GetAssetImage('avatar-placeholder.png')} />
                                                <p>Paul</p>
                                            </li>
                                            <li class="list-group-item">
                                                <img class="media-object circle" src={GetAssetImage('avatar-placeholder.png')} />
                                                <p>Alicia</p>
                                            </li>
                                        </ul> 

                                        <ul class="list-group w-100">
                                            <li class="list-group-item d-flex justify-content-between align-items-center title">
                                                Recent
                                            </li>
                                        </ul> */}

                                        <ul class="list-group w-100 recent">
                                            <li class="list-group-item selected">
                                                <div class="media">
                                                    <div class="media-left">
                                                        <a class="status" href="#"><img class="media-object circle" src={GetAssetImage('avatar-placeholder.png')} /><span
                                                            class="badge online"></span></a>
                                                    </div>
                                                    <div class="media-body">
                                                        <p class="media-heading">Laurentius Rando</p>
                                                        <p class="media-subheading">Tomorrow, I will need your help</p>
                                                    </div>
                                                    <div class="media-right">
                                                        <a href="#">
                                                            <span class="align-middle">8:00 PM</span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </li>

                                            <li class="list-group-item ">

                                                <div class="media">
                                                    <div class="media-left">
                                                        <a class="status" href="#"><img class="media-object circle" src={GetAssetImage('avatar-placeholder.png')} /><span
                                                            class="badge online"></span></a>
                                                    </div>
                                                    <div class="media-body">
                                                        <p class="media-heading">Laurentius Rando</p>
                                                        <p class="media-subheading">Tomorrow, I will need your help</p>
                                                    </div>
                                                    <div class="media-right">
                                                        <a href="#">
                                                            <span class="align-middle">8:00 PM</span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </li>

                                            <li class="list-group-item unread">
                                                <div class="media">
                                                    <div class="media-left">
                                                        <a class="status" href="#"><img class="media-object circle" src={GetAssetImage('avatar-placeholder.png')} /><span
                                                            class="badge online"></span></a>
                                                    </div>
                                                    <div class="media-body">
                                                        <p class="media-heading">Laurentius Rando</p>
                                                        <p class="media-subheading">Tomorrow, I will need  your help</p>
                                                    </div>
                                                    <div class="media-right">
                                                        <a href="#">
                                                            <span class="align-middle">8:00 PM</span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </li>

                                            <li class="list-group-item unread">
                                                <div class="media">
                                                    <div class="media-left">
                                                        <a class="status" href="#"><img class="media-object circle" src={GetAssetImage('avatar-placeholder.png')} /><span
                                                            class="badge online"></span></a>
                                                    </div>
                                                    <div class="media-body">
                                                        <p class="media-heading">Laurentius Rando</p>
                                                        <p class="media-subheading">Tomorrow, I will need  your help</p>
                                                    </div>
                                                    <div class="media-right">
                                                        <a href="#">
                                                            <span class="align-middle">8:00 PM</span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </li>

                                            <li class="list-group-item unread">
                                                <div class="media">
                                                    <div class="media-left">
                                                        <a class="status" href="#"><img class="media-object circle" src={GetAssetImage('avatar-placeholder.png')} /><span
                                                            class="badge online"></span></a>
                                                    </div>
                                                    <div class="media-body">
                                                        <p class="media-heading">Laurentius Rando</p>
                                                        <p class="media-subheading">Tomorrow, I will need  your help</p>
                                                    </div>
                                                    <div class="media-right">
                                                        <a href="#">
                                                            <span class="align-middle">8:00 PM</span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </li>

                                            <li class="list-group-item unread">
                                                <div class="media">
                                                    <div class="media-left">
                                                        <a class="status" href="#"><img class="media-object circle" src={GetAssetImage('avatar-placeholder.png')} /><span
                                                            class="badge online"></span></a>
                                                    </div>
                                                    <div class="media-body">
                                                        <p class="media-heading">Laurentius Rando</p>
                                                        <p class="media-subheading">Tomorrow, I will need your help</p>
                                                    </div>
                                                    <div class="media-right">
                                                        <a href="#">
                                                            <span class="align-middle">8:00 PM</span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </li>

                                            <li class="list-group-item unread">
                                                <div class="media">
                                                    <div class="media-left">
                                                        <a class="status" href="#"><img class="media-object circle" src={GetAssetImage('avatar-placeholder.png')} /><span
                                                            class="badge online"></span></a>
                                                    </div>
                                                    <div class="media-body">
                                                        <p class="media-heading">Laurentius Rando</p>
                                                        <p class="media-subheading">Tomorrow, I will need your help</p>
                                                    </div>
                                                    <div class="media-right">
                                                        <a href="#">
                                                            <span class="align-middle">8:00 PM</span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </li>

                                            <li class="list-group-item unread">
                                                <div class="media">
                                                    <div class="media-left">
                                                        <a class="status" href="#"><img class="media-object circle" src={GetAssetImage('avatar-placeholder.png')} /><span
                                                            class="badge online"></span></a>
                                                    </div>
                                                    <div class="media-body">
                                                        <p class="media-heading">Laurentius Rando</p>
                                                        <p class="media-subheading">Tomorrow, I will need your help</p>
                                                    </div>
                                                    <div class="media-right">
                                                        <a href="#">
                                                            <span class="align-middle">8:00 PM</span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </li>

                                            <li class="list-group-item unread">
                                                <div class="media">
                                                    <div class="media-left">
                                                        <a class="status" href="#"><img class="media-object circle" src={GetAssetImage('avatar-placeholder.png')} /><span
                                                            class="badge online"></span></a>
                                                    </div>
                                                    <div class="media-body">
                                                        <p class="media-heading">Laurentius Rando</p>
                                                        <p class="media-subheading">Tomorrow, I will need your help</p>
                                                    </div>
                                                    <div class="media-right">
                                                        <a href="#">
                                                            <span class="align-middle">8:00 PM</span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </li>

                                            <li class="list-group-item unread">
                                                <div class="media">
                                                    <div class="media-left">
                                                        <a class="status" href="#"><img class="media-object circle" src={GetAssetImage('avatar-placeholder.png')} /><span
                                                            class="badge online"></span></a>
                                                    </div>
                                                    <div class="media-body">
                                                        <p class="media-heading">Laurentius Rando</p>
                                                        <p class="media-subheading">Tomorrow, I will need your help</p>
                                                    </div>
                                                    <div class="media-right">
                                                        <a href="#">
                                                            <span class="align-middle">8:00 PM</span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </li>

                                            <li class="list-group-item unread">
                                                <div class="media">
                                                    <div class="media-left">
                                                        <a class="status" href="#"><img class="media-object circle" src={GetAssetImage('avatar-placeholder.png')} /><span
                                                            class="badge online"></span></a>
                                                    </div>
                                                    <div class="media-body">
                                                        <p class="media-heading">Laurentius Rando</p>
                                                        <p class="media-subheading">Tomorrow, I will need your help</p>
                                                    </div>
                                                    <div class="media-right">
                                                        <a href="#">
                                                            <span class="align-middle">8:00 PM</span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {/* <!-- end left column --> */}

                    {/* <!-- center column --> */}
                    <div class="col-sm empty-container-with-out-border center-column">

                        {/* <!-- chat container --> */}
                        <div class="row empty-inner-container-with-border chat-container">

                            <div class="container">
                                <div class="row">

                                    <ul class="list-group w-100">
                                        <li class="list-group-item d-flex justify-content-between align-items-center">
                                            <div class="media w-100">
                                                <div class="media-left mr-2">
                                                    <a href="#">
                                                        <img class="media-object circle" src={GetAssetImage('communities_image_placeholder.png')} alt="..." />
                                                    </a>
                                                </div>
                                                <div class="media-body">
                                                    <p class="media-heading">Margaret Tetcher</p>
                                                    <p class="media-subheading status"><span class="badge online"></span> Online</p>
                                                </div>
                                                <div class="media-right ">
                                                    <div class="dropdown pull-right ">
                                                        <button data-display="static" type="button" class="btn" data-toggle="dropdown">
                                                            <img src={GetAssetImage('vertical-dots.png')} />
                                                        </button>
                                                        <div class="dropdown-menu hasUpArrow dropdown-menu-right">
                                                            <a href="#" class="dropdown-item">
                                                                <img class="mr-1" src={GetAssetImage('edit-icon.png')} />
                                                                <span class="m-1">Edit</span></a>
                                                            <a href="#" class="dropdown-item">
                                                                <img class="mr-1" src={GetAssetImage('hide-visibility-icon.png')} />
                                                                <span class="m-1">Hide</span></a>
                                                            <a href="#" class="dropdown-item">
                                                                <img class="mr-1" src={GetAssetImage('save-icon.png')} />
                                                                <span class="m-1">Save</span></a>
                                                            <a href="#" class="dropdown-item">
                                                                <img class="mr-1" src={GetAssetImage('remove-icon.png')} />
                                                                <span class="m-1">Remove</span>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>

                                        <li class="list-group-item d-flex justify-content-between align-items-center chat-content-area">
                                            <div class="container p-0">

                                                {this.state.msgs.map(msg => (

                                                    <div class="row receiver">
                                                        <div class="media w-100 d-flex align-items-end">
                                                            <div class="media-left">
                                                                <a href="#">
                                                                    <img class="media-object circle" src={msg.userinfo.avatar || GetAssetImage('communities_image_placeholder.png')} alt="..." />
                                                                </a>
                                                            </div>
                                                            <div class="media-body">
                                                                <p class="chat-window">
                                                                    <span class="time">
                                                                        {moment(msg.createdAt).format('ddd, hh:mm')}
                                                                    </span>{msg.message}</p>
                                                            </div>
                                                            <div class="media-right">
                                                            </div>
                                                        </div>
                                                    </div>
                                                ))}

                                                {/* <div class="row sender">
                                                    <div class="media w-100 d-flex align-items-end">
                                                        <div class="media-left">
                                                        </div>
                                                        <div class="media-body">
                                                            <p class="chat-window"><span class="time">Tue, 23:01</span>Hi friend. Could you help me with something?</p>
                                                        </div>
                                                        <div class="media-right">
                                                            <a href="#">
                                                                <img class="media-object circle" src={GetAssetImage('communities_image_placeholder.png')} alt="..." />
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row sender">
                                                    <div class="media w-100 d-flex align-items-end">
                                                        <div class="media-left">
                                                        </div>
                                                        <div class="media-body">
                                                            <p class="chat-window"><span class="time">Tue, 23:01</span>Hi friend. Could you help me with something?</p>
                                                        </div>
                                                        <div class="media-right">
                                                            <a href="#">
                                                                <img class="media-object circle" src={GetAssetImage('communities_image_placeholder.png')} alt="..." />
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row receiver">
                                                    <div class="media w-100 d-flex align-items-end">
                                                        <div class="media-left">
                                                            <a href="#">
                                                                <img class="media-object circle" src={GetAssetImage('communities_image_placeholder.png')} alt="..." />
                                                            </a>
                                                        </div>
                                                        <div class="media-body">
                                                            <p class="chat-window"><span class="time">Tue, 23:01</span>Hi friend. Could you help me with something?</p>
                                                        </div>
                                                        <div class="media-right">
                                                        </div>
                                                    </div>
                                                </div> */}
                                            </div>
                                        </li>

                                        <li class="list-group-item d-flex justify-content-between align-items-center chat-box horizontal-line-top">
                                            <div class="container">
                                                <div class="row ">
                                                    <div class="media w-100 d-flex align-items-center">
                                                        <div class="media-left">
                                                            <a href="#">
                                                                <img class="media-object circle" src={GetAssetImage('smile.jpg')} alt="..." />
                                                            </a>
                                                        </div>
                                                        <div class="media-body">
                                                            <input
                                                                className=""
                                                                type="text"
                                                                placeholder="Type Something..."
                                                                name="msg"
                                                                value={this.state.msg}
                                                                onChange={(e) => this.handleChange(e)} />
                                                        </div>
                                                        <div class="media-right" onClick={this.sendMsg}>
                                                            <a href="#">
                                                                <img class="media-object circle" src={GetAssetImage('send.jpg')} alt="..." />
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        {/* <!-- end chat container --> */}

                    </div>
                    {/* <!-- end center column --> */}

                    {/* <!--  right column --> */}
                    <div class="col-sm empty-container-with-out-border right-column">
                        <div class="row empty-inner-container-with-border">

                            <div class="container">
                                <div class="row">
                                    <ul class="list-group w-100 select-profile">
                                        <li class="list-group-item">
                                            <img class="media-object circle" src={GetAssetImage('myfriends_image_placeholder.png')} alt="..." />
                                            <p class="heading">Media heading</p>
                                            <p class="subheading">Media heading</p>
                                            <button>Add</button>
                                        </li>
                                    </ul>
                                </div>

                                <div class="row empty-inner-container-with-out-border communities">
                                    <div class="container">
                                        <div class="row">

                                            <ul class="list-group w-100 m-0">
                                                <li class="list-group-item d-flex justify-content-between align-items-center pb-0">
                                                    Shared files
                                                    <button class="btn btn-secondary dropdown-toggle seemore pr-0" type="button">
                                                        Images
                                                    </button>
                                                </li>
                                            </ul>

                                            <ul class="list-group w-100 shared_files">
                                                <li class="list-group-item">
                                                    <a href="#">
                                                        <img class="media-object square" src={GetAssetImage('harley-davidson-zGzXsJUBQfs-unsplash@3x.png')} alt="..." />
                                                    </a>
                                                </li>
                                                <li class="list-group-item">
                                                    <a href="#">
                                                        <img class="media-object square" src={GetAssetImage('harley-davidson-zGzXsJUBQfs-unsplash@3x.png')} alt="..." />
                                                    </a>
                                                </li>
                                                <li class="list-group-item">
                                                    <a href="#">
                                                        <img class="media-object square" src={GetAssetImage('harley-davidson-zGzXsJUBQfs-unsplash@3x.png')} alt="..." />
                                                    </a>
                                                </li>
                                                <li class="list-group-item">
                                                    <a href="#">
                                                        <img class="media-object square" src={GetAssetImage('harley-davidson-zGzXsJUBQfs-unsplash@3x.png')} alt="..." />
                                                    </a>
                                                </li>
                                                <li class="list-group-item">
                                                    <a href="#">
                                                        <img class="media-object square" src={GetAssetImage('harley-davidson-zGzXsJUBQfs-unsplash@3x.png')} alt="..." />
                                                    </a>
                                                </li>
                                                <li class="list-group-item">
                                                    <a href="#">
                                                        <img class="media-object square" src={GetAssetImage('harley-davidson-zGzXsJUBQfs-unsplash@3x.png')} alt="..." />
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {/* <!-- end right column --> */}
                </div>
            </div>
        );
    }
}

export default Messenger;
