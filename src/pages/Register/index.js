import React from 'react';
import "../../assets/css/bootstrap.min.css";
import "../../assets/css/pull-push.css";
import "../../assets/css/fontawesome-all.css";
import "../../assets/css/style2.css";
import "../../assets/css/responsive.css";
import { Link } from 'react-router-dom';
import ListErrors from '../../components/Errors/ListErrors';
import { register } from '../../http/http-calls';
import { GetAssetImage } from '../../globalFunctions/';
import { switchLoader, alertBox } from '../../commonRedux/';
class Register extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      name: '',
      email: '',
      countryCode: '+44',
      phone: '',
      password: '',
      cPassword: '',
      errors: {},
      pwdtype: 'password',
      cpwdtype: 'password',
      countryCodes: [{ name: 'US', code: '+1' }, { name: 'IN', code: '+91' }, { name: 'GB', code: '+44' }]
    };
    this.handleChange = this.handleChange.bind(this);
    this.submitForm = this.submitForm.bind(this);
  }

  handleChange = (evt) => {
    const { name, value } = evt.target;
    this.setState({ [name]: value }, () => { });
  }

  handleValidation() {

    let fields = this.state,
      formIsValid = true,
      errors = {};

    if (!fields["name"]) {
      formIsValid = false;
      errors["name"] = "Cannot be empty";
    }

    if (typeof fields["name"] !== "undefined" && fields["name"]) {
      if (!fields["name"].match(/^[a-zA-Z]+$/)) {
        formIsValid = false;
        errors["name"] = "Only letters";
      }
    }

    if (!fields["email"]) {
      formIsValid = false;
      errors["email"] = "Cannot be empty";
    }

    if (typeof fields["email"] !== "undefined" && fields["email"]) {
      let lastAtPos = fields["email"].lastIndexOf('@');
      let lastDotPos = fields["email"].lastIndexOf('.');
      if (!(lastAtPos < lastDotPos && lastAtPos > 0 && fields["email"].indexOf('@@') == -1 && lastDotPos > 2 && (fields["email"].length - lastDotPos) > 2)) {
        formIsValid = false;
        errors["email"] = "Email is not valid";
      }
    }

    if (!fields["phone"]) {
      formIsValid = false;
      errors["phone"] = "Cannot be empty";
    }

    if (typeof fields["phone"] !== "undefined" && fields["phone"]) {
      if (!fields["phone"].match(/^[0-9]+$/) || fields["phone"].length != 10) {
        formIsValid = false;
        errors["phone"] = "Invalid Phone";
      }
    }

    if (!fields["password"]) {
      formIsValid = false;
      errors["password"] = "Cannot be empty";
    }
    if (!fields["cPassword"]) {
      formIsValid = false;
      errors["cPassword"] = "Cannot be empty";
    }
    if (fields["password"] != fields["cPassword"]) {
      formIsValid = false;
      errors["cPassword"] = "Password doesn't match";
    }

    this.setState({ errors: errors });
    return formIsValid;
  }

  submitForm = (e) => {

    if (this.handleValidation()) {
      let obj = {
        name: this.state.name,
        email: this.state.email,
        full_phone: `${this.state.countryCode}${this.state.phone}`,
        password: this.state.password,
        passwordrepeat: this.state.cPassword,
        group: 'customer',
        liquidity: '',
      };

      switchLoader(true, 'Please wait...');
      register(obj)
        .then(async resp => {
          switchLoader();
          alertBox(true, "Please verify your email", 'success');
        }, error => {
          switchLoader();
          let alert = error && error.data && error.data.message ? typeof error.data.message == 'object' ? Object.values(error.data.message).join(',') : error.data.message: '';
          alertBox(true, alert);
        });
    } else {
      console.log('Invalid form', this.state.errors);
    }
  }

  render() {

    const { email, name, countryCode, phone, password, cPassword, pwdtype, cpwdtype } = this.state;
    return (
      <React.Fragment>
        <div className="flexCenterBody">
          <main className="mainRow">

            <section className="mainRightSection loginSection">
              <div className="container-fluid">
                <div className="row">

                  <div className="container col-xl-4 col-lg-6 col-md-6 leftSection">
                    <div>
                      <a className="navbar-brand mb-2" href="#">
                        <img src={GetAssetImage('Rectangle_logo.png')} alt="" />
                      </a>
                    </div>
                    <div className="d-flex mb-3">
                      <div className="p-2 flex-fill ">

                        <h2 className="title">Sign Up</h2>
                        <div className="input-group mb-3">
                          <div className="input-group-prepend">
                            <span className="input-group-text"><i className="fas fa-user"></i></span>
                          </div>
                          <input
                            className="form-control"
                            type="text"
                            placeholder="Username"
                            name="name"
                            value={name}
                            onChange={(e) => this.handleChange(e)} />
                          <span style={{ color: "red", width: "100%" }}>{this.state.errors["name"]}</span>
                        </div>

                        <div className="input-group mb-3">
                          <div className="input-group-append">
                            <span className="input-group-text"><i className="far fa-envelope"></i></span>
                          </div>
                          <input
                            className="form-control"
                            type="email"
                            placeholder="Email"
                            name="email"
                            value={email}
                            onChange={(e) => this.handleChange(e)} />
                          <span style={{ color: "red", width: "100%" }}>{this.state.errors["email"]}</span>

                        </div>
                        <div className="input-group mb-3">
                          <div className="input-group-append">
                            <span className="input-group-text"><i className="fas fa-key"></i></span>
                          </div>
                          <input
                            className="form-control"
                            type={pwdtype}
                            placeholder="Password"
                            name="password"
                            value={password}
                            onChange={(e) => this.handleChange(e)} />
                          <span style={{ color: "red", width: "100%" }}>{this.state.errors["password"]}</span>
                          {(pwdtype == 'password') && <button type="button" className="showPW" onClick={(e) => this.setState({pwdtype: 'text'})}><i className="fa fa-fw fa-eye"></i></button>}
                          {(pwdtype == 'text') && <button type="button" className="showPW" onClick={(e) => this.setState({pwdtype: 'password'})}><i className="fa fa-fw fa-eye-slash"></i></button>}
                        </div>
                        <div className="input-group mb-3">
                          <div className="input-group-append">
                            <span className="input-group-text"><i className="fas fa-key"></i></span>
                          </div>
                          <input
                            className="form-control"
                            type={cpwdtype}
                            placeholder="Confirm Password"
                            name="cPassword"
                            value={cPassword}
                            onChange={(e) => this.handleChange(e)} />
                          <span style={{ color: "red", width: "100%" }}>{this.state.errors["cPassword"]}</span>
                          {(cpwdtype == 'password') && <button type="button" className="showPW" onClick={(e) => this.setState({cpwdtype: 'text'})}><i className="fa fa-fw fa-eye"></i></button>}
                          {(cpwdtype == 'text') && <button type="button" className="showPW" onClick={(e) => this.setState({cpwdtype: 'password'})}><i className="fa fa-fw fa-eye-slash"></i></button>}
                        </div>
                        <div className="input-group mb-3">
                          <div className="input-group-append">
                            <span className="input-group-text">
                              <select name="countryCode" id=""
                                onChange={(e) => this.handleChange(e)}
                                value={this.state.countryCode} >
                                {this.state.countryCodes.map((country, i) => (
                                  <option key={i} value={country.code} >
                                    {country.name} {country.code}
                                  </option>
                                ))}
                              </select>
                            </span>
                          </div>
                          <input
                            className="form-control"
                            type="text"
                            placeholder="Phone number"
                            name="phone"
                            value={phone}
                            onChange={(e) => this.handleChange(e)} />
                          <span style={{ color: "red", width: "100%" }}>{this.state.errors["phone"]}</span>

                        </div>
                        <div className="input-group mb-3">
                          <div className="robotCheck">
                            <label className="customCheck">I'm not a robot
                                <input type="checkbox" checked="checked" />
                              <span className="checkmark"></span>
                            </label>
                          </div>
                        </div>
                        <div className="input-group mb-3 ">
                          <div className="input-group-append nextbtn">
                            <Link to="/login">Login instead</Link>
                            <button
                              className="btn"
                              type="submit"
                              disabled={this.props.inProgress}
                              onClick={(e) => this.submitForm(e)}
                            >
                              Sign up
                              </button>
                          </div>
                          <div className="twitter">
                            <button className="btn"><i className="fab fa-twitter"></i> Sign up with Twitter</button>
                          </div>
                          <p className="term">By continuing, you're confirming that
                    you've read our<span data-toggle="modal" data-target="#termsPoliciesModal">Terms & Conditions and
                      Cookie Policy</span> </p>
                        </div>
                      </div>

                    </div>
                  </div>
                  <div className="container-fluid col-xl-8 col-lg-6 col-md-6 bgImg"></div>
                </div>
              </div>
            </section>

          </main>
        </div>

        {/* <div className="auth-page">
        <div className="container page">
          <div className="row">

            <div className="col-md-6 offset-md-3 col-xs-12">
              <h1 className="text-xs-center">Sign Up</h1>
              <p className="text-xs-center">
                <Link to="/login">
                  Have an account?
                </Link>
              </p>

              <ListErrors errors={this.props.errors} />

              <fieldset>

                <fieldset className="form-group">
                  <input
                    className="form-control form-control-lg"
                    type="text"
                    placeholder="Username"
                    name="name"
                    value={name}
                    onChange={(e) => this.handleChange(e)} />
                </fieldset>

                <fieldset className="form-group">
                  <input
                    className="form-control form-control-lg"
                    type="email"
                    placeholder="Email"
                    name="email"
                    value={email}
                    onChange={(e) => this.handleChange(e)} />
                </fieldset>

                <fieldset className="form-group">
                  <input
                    className="form-control form-control-lg"
                    type="text"
                    placeholder="Phone"
                    name="phone"
                    value={phone}
                    onChange={(e) => this.handleChange(e)} />
                </fieldset>

                <fieldset className="form-group">
                  <input
                    className="form-control form-control-lg"
                    type="password"
                    placeholder="Password"
                    name="password"
                    value={password}
                    onChange={(e) => this.handleChange(e)} />
                </fieldset>

                <fieldset className="form-group">
                  <input
                    className="form-control form-control-lg"
                    type="password"
                    placeholder="Confirm Password"
                    name="cPassword"
                    value={cPassword}
                    onChange={(e) => this.handleChange(e)} />
                </fieldset>

                <button
                  className="btn btn-lg btn-primary pull-xs-right"
                  type="submit"
                  disabled={this.props.inProgress}
                  onClick={(e) => this.submitForm(e)}
                >
                  Sign up
                  </button>

              </fieldset>
            </div>

          </div>
        </div>
      </div> */}
      </React.Fragment>
    );
  }
}

export default Register;
