export const GetAssetImage = (imageName = 'noImage.jpg') => {
    return require("../assets/images/"+imageName);
};
